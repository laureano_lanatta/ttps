<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="css/styles.css" /> 
	<title>Cobertura</title>
</head>
<body>
	<h1>Datos de la Cobertura</h1>
	<form action="DatosCobertura" method="post">
		<label for="tipoCobertura">Tipo de cobertua</label><br>
		<input type="radio" name="tipoCobertura" value="tc" checked />Tercero completo<br>
		<input type="radio" name="tipoCobertura" value="tdf" />Todo riesgo con franquicia<br>
		<input type="radio" name="tipoCobertura" value="tdt" />Todo riesgo total<br>
		<input type="submit" value="Siguiente" />
	</form>
</body>
</html>