<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="css/styles.css" /> 
	<title>Cotizacion</title>
</head>
<body>
	<h1>Cotizacion</h1>
	<h3>Marca: </h3>${cotizacion.marca}<br>
	<h3>Modelo: </h3>${cotizacion.modelo}<br>
	<h3>Año vehiculo: </h3>${cotizacion.yearVehiculo}<br>
	<h3>Km/año: </h3>${cotizacion.kmYear}<br>
	<h3>Dueño: </h3>${cotizacion.owner}<br>
	<h3>Sexo: </h3>${cotizacion.sexo}<br>
	<h3>Nacimiento: </h3>${cotizacion.nacimiento}<br>
	<h3>Telefono: </h3>${cotizacion.telefono}<br>
	<h3>Email: </h3>${cotizacion.email}<br>
	<h3>Tipo de cobertura: </h3>${cotizacion.tipoCobertura}<br>
	<h3>Cotizacion: </h3>${cotizacion.calcularCotizacion()}
</body>
</html>