<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="css/styles.css" /> 
	<title>Auto</title>
</head>
<body>
<h1>Datos del Auto</h1>
<form action="DatosAuto" method="post">
	<label for="marca">Marca</label>
	<input type="text" name="marca" /><br>
	<label for="modelo">Modelo</label>
	<input type="text" name="modelo" /><br>
	<label for="yearVehiculo">Año</label>
	<select name="yearVehiculo">
		<% for (int i = 1990; i < 2017; i++) { %>
			<option value="<%= i %>"> <%= i %></option>
		<% } %>
	</select><br>
	<label for="kmYear">Km/año</label>
	<select name="kmYear">
		<% for (int i = 1; i < 11; i++) { %>
			<option value="<%= i * 5000 %>">Hasta <%= i * 5000 %></option>
		<% } %>
	</select><br>
	<input type="submit" value="Siguiente" />
</form>
</body>
</html>