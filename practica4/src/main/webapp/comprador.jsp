<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<% if (session.isNew()) { %>	
	<jsp:forward page="auto.jsp" />
<% } %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="css/styles.css" />
	<title>Comprador</title>
</head>
<body>
<h1>Datos del Comprador</h1>
<form action="DatosComprador" method="post">
	<label for="owner">Dueño</label>
	<input type="text" name="owner" /><br>
	<label for="sexo">Sexo</label><br>
	<input type="radio" name="sexo" value="Masculino" />Masculino<br>
	<input type="radio" name="sexo" value="Femenino" />Femenino<br>
	<label for="nacimiento">Nacimiento</label>
	<input type="date" name="nacimiento" /><br>
	<label for="telefono">Telefono</label>
	<input type="text" name="telefono" /><br>
	<label for="email">Email</label>
	<input type="text" name="email" /><br>
	<input type="submit" value="Siguiente" />
</form>
</body>
</html>