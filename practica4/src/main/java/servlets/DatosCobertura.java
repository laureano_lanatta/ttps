package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import classes.Cotizacion;

@WebServlet("/DatosCobertura")
public class DatosCobertura extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		if (session.isNew())
			response.sendRedirect("auto.jsp");
		
		Cotizacion cotizacion = (Cotizacion)session.getAttribute("cotizacion");
		
		cotizacion.setTipoCobertura(request.getParameter("tipoCobertura"));
		
		response.sendRedirect("cotizacion.jsp");
		
	}

}
