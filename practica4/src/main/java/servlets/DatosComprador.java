package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import classes.Cotizacion;

@WebServlet("/DatosComprador")
public class DatosComprador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		if (session.isNew())
			response.sendRedirect("auto.jsp");
		
		Cotizacion cotizacion = (Cotizacion)session.getAttribute("cotizacion");
		
		cotizacion.setOwner(request.getParameter("owner"));
		cotizacion.setSexo(request.getParameter("sexo"));
		cotizacion.setNacimiento(request.getParameter("nacimiento"));
		cotizacion.setTelefono(request.getParameter("telefono"));
		cotizacion.setEmail(request.getParameter("email"));
		
		response.sendRedirect("cobertura.jsp");
		
	}

}
