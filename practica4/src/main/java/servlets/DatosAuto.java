package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import classes.Cotizacion;

@WebServlet("/DatosAuto")
public class DatosAuto extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cotizacion cotizacion = new Cotizacion();
		HttpSession session = request.getSession();
		session.setAttribute("cotizacion", cotizacion);
		
		cotizacion.setMarca(request.getParameter("marca"));
		cotizacion.setModelo(request.getParameter("modelo"));
		cotizacion.setYearVehiculo(request.getParameter("yearVehiculo"));
		cotizacion.setKmYear(request.getParameter("kmYear"));
		
		response.sendRedirect("comprador.jsp");
		
	}

}
