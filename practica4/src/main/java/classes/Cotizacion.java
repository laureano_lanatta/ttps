package classes;

import java.io.Serializable;

public class Cotizacion implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String marca;
	private String modelo;
	private String yearVehiculo; //Date?
	private String kmYear; //int?
	private String owner;
	private String sexo; //Char?
	private String nacimiento; //Date?
	private String telefono;
	private String email;
	private String tipoCobertura;
	
	public Cotizacion() {}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getYearVehiculo() {
		return yearVehiculo;
	}
	
	public void setYearVehiculo(String yearVehiculo) {
		this.yearVehiculo = yearVehiculo;
	}
	
	public String getKmYear() {
		return kmYear;
	}
	
	public void setKmYear(String kmYear) {
		this.kmYear = kmYear;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public String getSexo() {
		return sexo;
	}
	
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public String getNacimiento() {
		return nacimiento;
	}
	
	public void setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTipoCobertura() {
		return tipoCobertura;
	}
	
	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	
	public float calcularCotizacion() {
		return (float)5.24;
	}
		
}
