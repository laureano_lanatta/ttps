package ttps.spring.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import ttps.spring.model.Usuario;
import ttps.spring.model.dao.impl.TestDAOHibernateJpa;

@Service
public class UsuarioService {

	@Autowired
	private TestDAOHibernateJpa dao;

	@Transactional
	public Usuario get(Long id) {
		return dao.get(id);
	}
	
}
