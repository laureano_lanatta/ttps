package ttps.spring.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ttps.spring.model.Usuario;
import ttps.spring.model.dao.impl.TestDAOHibernateJpa;
import ttps.spring.model.dao.impl.UsuarioDAOHibernateJPA;
import ttps.spring.services.UsuarioService;

@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TestDAOHibernateJpa dao = new TestDAOHibernateJpa();
		/*
		if (dao.getEntityManager() == null)
			System.out.println("null");
		else
			System.out.println("not null");
		*/
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
