package ttps.spring.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AutenticacionController {

	@PostMapping(path = "/autenticacion")
	public ResponseEntity<Void> autenticar(@RequestHeader("usuario") String usuario, @RequestHeader("clave") String clave) {
		if (usuario.equals("root") && clave.equals("pass")) {
			ResponseEntity<Void> response = new ResponseEntity<Void>(HttpStatus.OK);
			response.getHeaders().add("token", "12456");
			return response;
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
}
