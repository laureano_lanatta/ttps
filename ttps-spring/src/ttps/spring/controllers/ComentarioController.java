package ttps.spring.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ttps.spring.model.Comentario;
import ttps.spring.model.dao.ComentarioDAO;
import ttps.spring.model.dto.ComentarioDTO;

@RestController
public class ComentarioController {

	@Autowired
	ComentarioDAO comentarioDAO;
	
	@GetMapping(path = "/comentarios")
	public List<ComentarioDTO> getComentarios() {
		return this.comentarioDAO.getAll()
								 .stream()
								 .map(comentario -> new ComentarioDTO(comentario))
								 .collect(Collectors.toList());
	}
	
	@GetMapping(path = "/comentarios/{id}")
	public ComentarioDTO getComentario(@PathVariable("id") long id) {
		return new ComentarioDTO(this.comentarioDAO.get(id));
	}
	
	@PostMapping(path = "/comentarios")
	public Comentario createComentario(@RequestBody Comentario comentario) {
		return this.comentarioDAO.save(comentario);
	}
	
}
