package ttps.spring.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ttps.spring.model.Usuario;
import ttps.spring.model.dao.UsuarioDAO;
import ttps.spring.model.dto.UsuarioDTO;

@RestController
public class UsuarioController {

	@Autowired
	UsuarioDAO usuarioDAO;	
	
	@GetMapping(path = "/usuarios")
	@Transactional
	public List<UsuarioDTO> getUsuarios() {
		return this.usuarioDAO.getAll()
							  .stream()
							  .map(usuario -> new UsuarioDTO(usuario))
							  .collect(Collectors.toList());
	}
	
	@GetMapping(path = "/usuarios/{id}")
	public UsuarioDTO getUsuario(@PathVariable("id") long id) {
		return new UsuarioDTO(this.usuarioDAO.get(id));
	}
	
	@PostMapping(path = "/usuarios")
	public Usuario createUser(@RequestBody Usuario usuario) {
		return this.usuarioDAO.save(usuario);
	}
	
}
