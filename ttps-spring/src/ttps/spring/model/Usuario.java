package ttps.spring.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Usuario {

	@Id
	@GeneratedValue
	private Long id;

	private String nombre;

	@OneToMany(mappedBy = "usuario", orphanRemoval = true)
	private Set<Comentario> comentarios = new HashSet<Comentario>();

	public Usuario() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(Set<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	@Override
	public boolean equals(Object o) {
		return o != null && ((Usuario) o).getId().equals(this.getId());
	}

	@Override
	public String toString() {
		return "ID: " + this.getId() + ", NOMBRE: " + this.getNombre();
	}

}
