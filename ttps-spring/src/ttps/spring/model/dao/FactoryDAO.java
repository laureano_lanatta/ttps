package ttps.spring.model.dao;

import org.springframework.stereotype.Component;

import ttps.spring.model.dao.impl.*;

public class FactoryDAO {
	
	public static UsuarioDAO getUsuarioDAO() {
		return new UsuarioDAOHibernateJPA();
	}
	
	public static ComentarioDAO getComentarioDAO() {
		return new ComentarioDAOHibernateJPA();
	}
	
}
