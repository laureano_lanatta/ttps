package ttps.spring.model.dao.impl;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import ttps.spring.model.Usuario;

@Repository
public class TestDAOHibernateJpa {
	
	//Aca no anda (?!)
	private EntityManager entityManager;
	
	public TestDAOHibernateJpa() {
		
	}
	
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		System.out.println("<<<<< ---SETTING ENTITYMANAGER--- >>>>>");
		System.out.println("EntityManager: <" + em + ">");
		this.entityManager = em;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public Usuario get(Serializable id) {
		return this.getEntityManager().find(Usuario.class, id);
	}
	
	public void echo(boolean withGetter) {
		if (withGetter)
			System.out.println(this.getEntityManager());
		else
			System.out.println(this.entityManager);
	}

}
