package ttps.spring.model.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttps.spring.model.Usuario;
import ttps.spring.model.dao.UsuarioDAO;

@Transactional
@Repository
public class UsuarioDAOHibernateJPA extends GenericDAOHibernateJPA<Usuario> implements UsuarioDAO {
	
	public UsuarioDAOHibernateJPA() {
		super(Usuario.class);
	}

}
