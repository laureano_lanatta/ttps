package ttps.spring.model.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import ttps.spring.model.dao.GenericDAO;

@Transactional
public abstract class GenericDAOHibernateJPA<T> implements GenericDAO<T> {

	protected Class<T> persistentClass;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public GenericDAOHibernateJPA(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}
	
	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	public void setPersistentClass(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	public void setEntityManager(EntityManager em){
		this.entityManager = em;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public T get(Serializable id) {
		return this.getEntityManager().find(this.getPersistentClass(), id);
	}

	@Override
	public List<T> getAll() {
		EntityManager entityManager = this.getEntityManager();
		String query = "FROM " + this.getPersistentClass().getSimpleName();
		List<T> result = null;
		try {
			result = entityManager.createQuery(query, this.getPersistentClass()).getResultList();
		} catch (RuntimeException e) {
			System.out.println("ERROR: " + e.getMessage());
		} finally {
			entityManager.close();
		}
		return result;
	}
	
	@Override
	public boolean exists(Serializable id) {
		EntityManager entityManager = this.getEntityManager();
		try {
			boolean exists = this.get(id) != null;
			entityManager.close();
			return exists;
		} catch (RuntimeException e) {
			System.out.println("ERROR: " + e.getMessage());
			throw e;
		}
	}

	@Override
	@Transactional
	public T save(T entity) {
		return this.getEntityManager().merge(entity);
	}

	@Override
	public T update(T entity) {
		EntityManager entityManager = this.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.merge(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if (transaction != null && transaction.isActive())
				transaction.rollback();
			System.out.println("ERROR: " + e.getMessage());
			throw e;
		} finally {
			entityManager.close();
		}
		return entity;
	}

	@Override
	public void delete(T entity) {
		EntityManager entityManager = this.getEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.remove(entityManager.contains(entity) ? entity
																: entityManager.merge(entity));
			transaction.commit();
		} catch (RuntimeException e) {
			if (transaction != null && transaction.isActive())
				transaction.rollback();
			System.out.println("ERROR: " + e.getMessage());
			throw e;
		} finally {
			entityManager.close();
		}
	}

	@Override
	public T delete(Serializable id) {
		T entity = this.get(id);
		this.delete(entity);
		return entity;
	}

}
