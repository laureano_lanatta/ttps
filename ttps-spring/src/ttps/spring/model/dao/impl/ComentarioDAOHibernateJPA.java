package ttps.spring.model.dao.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttps.spring.model.Comentario;
import ttps.spring.model.dao.ComentarioDAO;

@Transactional
@Repository
public class ComentarioDAOHibernateJPA extends GenericDAOHibernateJPA<Comentario> implements ComentarioDAO {

	public ComentarioDAOHibernateJPA() {
		super(Comentario.class);
	}

}
