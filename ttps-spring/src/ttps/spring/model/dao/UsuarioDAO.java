package ttps.spring.model.dao;

import ttps.spring.model.Usuario;

public interface UsuarioDAO extends GenericDAO<Usuario> {

}
