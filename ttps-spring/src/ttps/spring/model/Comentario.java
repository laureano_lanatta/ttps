package ttps.spring.model;

import javax.persistence.*;

@Entity
public class Comentario {

	@Id
	@GeneratedValue
	private Long id;
	
	
	private String mensaje;
	
	@ManyToOne(optional = false)
	@JoinColumn
	private Usuario usuario;
	
	
	public Comentario() {
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public boolean equals(Object o) {
		return o != null && ((Comentario)o).getId().equals(this.getId());
	}
	
	@Override
	public String toString() {
		return "ID: " + this.getId() + ", MENSAJE: " + this.getMensaje();
	}
	
}
