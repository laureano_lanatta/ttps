package ttps.spring.model.dto;

import java.util.Set;
import java.util.stream.Collectors;

import ttps.spring.model.Comentario;
import ttps.spring.model.Usuario;

public class UsuarioDTO {

	private Long id;
	private String nombre;
	private Set<Long> idComentarios;

	public UsuarioDTO(Usuario u) {
		this.id = u.getId();
		this.nombre = u.getNombre();
		this.idComentarios = u.getComentarios()
							  .stream()
							  .map(Comentario::getId)
							  .collect(Collectors.toSet());
		
	}	

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String nombre) {
		this.nombre = nombre;
	}

	public String getUsername() {
		return nombre;
	}
	
	public void setComentarios(Set<Long> idComentarios) {
		this.idComentarios = idComentarios;
	}

	public Set<Long> getComentarios() {
		return idComentarios;
	}
	
}
