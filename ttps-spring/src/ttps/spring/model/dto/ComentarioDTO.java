package ttps.spring.model.dto;

import ttps.spring.model.Comentario;

public class ComentarioDTO {

	private Long id;
	private String mensaje;
	private Long idUsuario;
	
	public ComentarioDTO(Comentario c) {
		this.id = c.getId();
		this.mensaje = c.getMensaje();
		this.idUsuario = c.getUsuario().getId();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public Long getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
}
