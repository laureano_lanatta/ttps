package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import blogDeMensajes.Message;
import blogDeMensajes.User;
import dao.FactoryDAO;
import dao.MessageDAO;

@WebServlet("/SaveMessage")
public class SaveMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Que parte aca y que parte en el DAO (?)
		String body = request.getParameter("messageBody");
		User user = (User)request.getSession().getAttribute("user");
		Message message = new Message(body, user);
		MessageDAO messageDAO = FactoryDAO.getMessageDAO();
		messageDAO.save(message);
		response.sendRedirect("visualizarMensajes.jsp");
	}

}
