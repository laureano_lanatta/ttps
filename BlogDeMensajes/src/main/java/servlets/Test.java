package servlets;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.persistence.*;
import blogDeMensajes.Message;

@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Entity Manager Factory...");
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnit");
		
		System.out.println("Entity Manager...");
		EntityManager em = emf.createEntityManager();
		
		System.out.println("Query...");
		List<Message> messages = (List<Message>)(em.createQuery("from Message m")).getResultList();
		
		System.out.println("Data...");
		for (Message msj : messages) {
			System.out.println(msj.getAuthor() + " dice: " + msj);
		}
		
		System.out.println("Entity Manager Close...");
		em.close();
		
		System.out.println("Entity Manager Factory Close...");
		emf.close(); //Cuando? "Cleaning up connection pool". Listener (up/down)? Servlet(init/destroy)?
		
		System.out.println("Done.");
	}

}
