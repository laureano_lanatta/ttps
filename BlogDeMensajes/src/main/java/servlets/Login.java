package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import blogDeMensajes.User;
import dao.FactoryDAO;
import dao.UserDAO;

@WebServlet("/Login")
public class Login extends HttpServlet {
	
	private static final long serialVersionUID = 1L;	
	
	public void init() throws ServletException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDAO userDAO = FactoryDAO.getUserDAO();
		User user = userDAO.getByNameAndPass(request.getParameter("userName"), request.getParameter("userPass"));
		if (user == null)
			response.sendRedirect("notLogged.jsp");
		else {
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			response.sendRedirect("visualizarMensajes.jsp");
		}
	}

}
