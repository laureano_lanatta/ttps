package dao;

import java.util.List;
import blogDeMensajes.Message;
import blogDeMensajes.User;

public interface MessageDAO {

	public Message get(int id);
	public List<Message> getAll();
	public List<Message> getByUser(User user);
	public void save(Message message);
	public void delete(Message message);
	
}
