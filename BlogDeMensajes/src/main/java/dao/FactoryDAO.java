package dao;

//import dao.jdbc.*;
import dao.jpa.*;

public class FactoryDAO {

	public static UserDAO getUserDAO() {
		//return new UserDAOjdbc();
		return new UserDAOjpa();
	}
	
	public static MessageDAO getMessageDAO() {
		//return new MessageDAOjdbc();
		return new MessageDAOjpa();
	}
	
}
