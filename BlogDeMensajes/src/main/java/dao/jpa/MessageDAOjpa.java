package dao.jpa;

import dao.MessageDAO;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import blogDeMensajes.*;

public class MessageDAOjpa implements MessageDAO {

	@Override
	public Message get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Message> getAll() {
		List<Message> messages = new ArrayList<Message>();
		try {
			EntityManager entityManager = Persistence.createEntityManagerFactory("persistenceUnit")
								 					 .createEntityManager(); //javax.persistence.PersistenceException
			messages = entityManager.createQuery("SELECT m FROM Message m", Message.class).getResultList(); //java.lang.IllegalArgumentException
			entityManager.close();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			System.out.println("Type: " + e.getClass());
		}
		return messages;
	}

	@Override
	public List<Message> getByUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Message message) {
		try {
			EntityManager entityManager = Persistence.createEntityManagerFactory("persistenceUnit")
					 								 .createEntityManager();
			EntityTransaction entityManagerTransaction = entityManager.getTransaction();
			entityManagerTransaction.begin();
			entityManager.persist(message);
			entityManagerTransaction.commit();
			entityManager.close();	
		}  catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			System.out.println("Type: " + e.getClass());
		}	
	}

	@Override
	public void delete(Message message) {
		// TODO Auto-generated method stub
		
	}

}
