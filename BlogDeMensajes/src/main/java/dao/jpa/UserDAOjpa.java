package dao.jpa;

import java.util.List;
import javax.persistence.*;
import blogDeMensajes.*;
import dao.UserDAO;

public class UserDAOjpa implements UserDAO {

	@Override
	public User getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getByNameAndPass(String name, String pass) {
		User user = null;
		// Tratamiento diferente [si no pudo chequear] o [si las credenciales son incorrectas] (?). Ambos retornan null.
		try {
			EntityManager entityManager = Persistence.createEntityManagerFactory("persistenceUnit")
								 					 .createEntityManager();
			TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.name = :name AND u.pass = :pass", User.class)
												  .setParameter("name", name)
												  .setParameter("pass", pass);
			user = query.getResultList()
						.stream()
						.findFirst()
						.orElse(null);
			
			entityManager.close();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			System.out.println("Type: " + e.getClass());
		}
		return user;
	}

	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		
	}

}
