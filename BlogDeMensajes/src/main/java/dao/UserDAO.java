package dao;

import java.util.List;
import blogDeMensajes.User;

public interface UserDAO {

	public User getById(int id);
	public User getByName(String name);
	public User getByNameAndPass(String name, String pass);
	public List<User> getAll();
	public void save(User user);
	public void delete(User user);
	
}
