package dao.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import blogDeMensajes.Message;
import blogDeMensajes.User;
import dao.FactoryDAO;
import dao.MessageDAO;
import dao.MyDataSource;
import dao.UserDAO;

public class UserDAOjdbc implements UserDAO {

	@Override
	public User getById(int id) {
		User user = null;
		try {
			Connection connection = MyDataSource.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM User u WHERE u.id = '" + id + "'");
			if (result.next() == true) {
				String userName = result.getString(2);
				String userPass = result.getString(3);
				user = new User(userName, userPass);
				// ?>
				int userId = result.getInt(1);
				user.setId(userId);
				List<Message> messages = new ArrayList<Message>();
				MessageDAO messageDAO = FactoryDAO.getMessageDAO();
				messages = messageDAO.getByUser(user);
				user.setMessages(messages);
				// ?<
			}
			result.close();
			statement.close();
			connection.close();
		}  catch (SQLException e) {
			System.out.println("SQL Error: " + e.getMessage());
		}
		return user;
	}

	@Override
	public User getByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getByNameAndPass(String name, String pass) {
		User user = null;
		try {
			Connection connection = MyDataSource.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM User u WHERE u.name = '" + name + "' AND u.pass = '" + pass + "'");
			if (result.next() == true) {
				String userName = result.getString(2);
				String userPass = result.getString(3);
				user = new User(userName, userPass);
				// ?>
				int userId = result.getInt(1);
				user.setId(userId);
				List<Message> messages = new ArrayList<Message>();
				MessageDAO messageDAO = FactoryDAO.getMessageDAO();
				messages = messageDAO.getByUser(user);
				user.setMessages(messages);
				// ?<
			}
			result.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Error: " + e.getMessage());
		}
		return user;
	}
	
	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		
	}

}
