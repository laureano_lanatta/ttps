package dao.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import blogDeMensajes.Message;
import blogDeMensajes.User;
import dao.FactoryDAO;
import dao.MessageDAO;
import dao.MyDataSource;
import dao.UserDAO;

public class MessageDAOjdbc implements MessageDAO {

	@Override
	public Message get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Message> getAll() {
		List<Message> messages = new ArrayList<Message>();
		try {
			Connection connection = MyDataSource.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM Message m");
			if (result.next() == true) {
				do {
					String body = result.getString(2);
					int authorId = result.getInt(3);
					UserDAO userDAO = FactoryDAO.getUserDAO();
					// ? Cuando pide un user llama a getByUser que carga los mensajes otra vez (para ese usuario)
					// ? Levanta todos los mensajes 2 veces!
					User user = userDAO.getById(authorId);
					Message message = new Message(body, user);
					messages.add(message);
				} while (result.next() == true);
			}
			result.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Error: " + e.getMessage());
		}
		return messages;
	}

	@Override
	public List<Message> getByUser(User user) {
		List<Message> messages = new ArrayList<Message>();
		try {
			Connection connection = MyDataSource.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM Message m WHERE m.idUser = '" + user.getId() + "'");
			if (result.next() == true) {
				do {
					String body = result.getString(2);
					Message message = new Message(body, user);
					messages.add(message);
				} while (result.next() == true);
			}
			result.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Error: " + e.getMessage());
		}
		return messages;
	}
	
	@Override
	public void save(Message message) {
		try {
			Connection connection = MyDataSource.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			statement.executeUpdate("INSERT INTO Message (body, idUser) values('" + message.getBody() + "', " + message.getAuthor().getId() + ")");
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Error: " + e.getMessage());
		}
	}

	@Override
	public void delete(Message message) {
		// TODO Auto-generated method stub
		
	}

}
