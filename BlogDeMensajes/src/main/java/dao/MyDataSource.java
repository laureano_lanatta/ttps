package dao;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class MyDataSource {

	private static DataSource dataSource = null;
	
	static {
		try {
			dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/BlogDeMensajes");
		} catch (NamingException e) {
			System.out.println("Naming Error: " + e.getMessage());
		}
	}

	public static DataSource getDataSource() {
		return dataSource;
	}

}
