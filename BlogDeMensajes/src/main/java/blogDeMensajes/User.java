package blogDeMensajes;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {

	@Id @GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "pass")
	private String pass;
	
	@OneToMany(mappedBy="author")
	private List<Message> messages;
	
	public User() {
		this.messages = new ArrayList<Message>();
	}
	
	public User(String aName, String aPass) {
		this();
		this.name = aName;
		this.pass = aPass;
		//this.messages = this.loadMessages();	// ?
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPass() {
		return this.pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public List<Message> getMessages() {
		return this.messages;
	}
	
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	@Override
	public boolean equals(Object aUser) {
		return ((User)aUser).getName().equals(this.getName())
				&& ((User)aUser).getPass().equals(this.getPass());
	}
	
	@Override
	public String toString() {
		return "<" + this.getName() + ">";
	}
	
}
