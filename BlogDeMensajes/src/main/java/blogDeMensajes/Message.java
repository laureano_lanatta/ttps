package blogDeMensajes;

import javax.persistence.*;

@Entity
public class Message {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "body")
	private String body;

	@ManyToOne
	@JoinColumn(name = "idUser")
	private User author;
	
	public Message() {
		
	}
	
	public Message(String aText, User anAuthor) {
		this.body = aText;
		this.author = anAuthor;
	}
	
	public String getBody() {
		return this.body;
	}
	
	public void setBody(String aText) {
		this.body = aText;
	}
	
	public User getAuthor() {
		return this.author;
	}
	
	public void setAuthor(User anAuthor) {
		this.author = anAuthor;
	}
	
	@Override
	public String toString() {
		return "\"" + this.getBody() + "\"";
	}
	
}
