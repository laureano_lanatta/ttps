package filters;

import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//Necesidad de directiva session="false" en jsp.
//Es esta la forma de controlar el acceso a paginas/servlets con respecto a la sesion?
@WebFilter(
		dispatcherTypes = {DispatcherType.REQUEST }
					, 
		urlPatterns = { 
				"/salir.jsp", 
				"/agregarMensaje.jsp",
				"/Logout",     //Estaba en servletNames. Get.
				"/SaveMessage" //(?)
		}, 
		servletNames = { 
				"SaveMessage", //Si nunca entra por get (urlPatterns) y la unica forma
				"Logout"       //de alcanzar el servlet es desde una jsp que valida la
		})					   //session (urlPatterns), es necesario que esté acá?
public class LoggedUser implements Filter {
    
	FilterConfig config;
	
	public LoggedUser() {
        
    }

	public void destroy() {
		this.config = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		HttpSession session = req.getSession();
		//Esto es una basura. Si no tengo atributos?
		//Session.isNew() parece inservible con jsp.
		if (session.getAttribute("user") == null)
			res.sendRedirect("notLogged.jsp");
		else
			chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		this.config = fConfig;
	}

}
