package listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.List;
import java.util.ArrayList;
import blogDeMensajes.User;

@WebListener
public class UsersLoading implements ServletContextListener {

    public UsersLoading() {
        
    }

    public void contextDestroyed(ServletContextEvent event)  { 
         
    }

    public void contextInitialized(ServletContextEvent event)  { 
         List<User> users = new ArrayList<User>();
         /*
         users.add(new User("Usuario A", "a"));
         users.add(new User("Usuario B", "b"));
         users.add(new User("Usuario C", "c"));
         users.add(new User("Usuario D", "d"));
         */
         event.getServletContext().setAttribute("users", users);
    }
	
}
