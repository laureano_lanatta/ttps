package listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.List;
import java.util.ArrayList;
import blogDeMensajes.Message;
import blogDeMensajes.User;

@WebListener
public class MessagesLoading implements ServletContextListener {

    public MessagesLoading() {
        
    }

    public void contextDestroyed(ServletContextEvent event)  { 
         
    }

    public void contextInitialized(ServletContextEvent event)  { 
        List<Message> messages = new ArrayList<Message>();
        /*
        messages.add(new Message("Un mensaje del usuario x.", new User("Usuario X", "x")));
        messages.add(new Message("Un mensaje del usuario y.", new User("Usuario Y", "y")));
        messages.add(new Message("Un mensaje del usuario z.", new User("Usuario Z", "z")));
        messages.add(new Message("Un mensaje del usuario w.", new User("Usuario W", "w")));
    	*/
    	event.getServletContext().setAttribute("messages", messages);
    }
	
}
