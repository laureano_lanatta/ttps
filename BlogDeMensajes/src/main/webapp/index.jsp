<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>Documentos</title>
</head>
<body>
	<div class="container h-100">
		<div class="row h-100 justify-content-center align-items-center">
			<div class="col-auto">
				<h1 class="display-3 text-center">Documentos</h1>
				<a class="btn btn-light btn-lg btn-block" href="visualizarMensajes.jsp" role="button">visualizarMensajes.jsp</a>
				<a class="btn btn-light btn-lg btn-block" href="login.jsp" role="button">login.jsp</a>
				<a class="btn btn-light btn-lg btn-block" href="agregarMensaje.jsp" role="button">agregarMensaje.jsp</a>
				<a class="btn btn-light btn-lg btn-block" href="salir.jsp" role="button">salir.jsp</a>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>