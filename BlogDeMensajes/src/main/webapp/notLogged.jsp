<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>No Logueado!</title>
</head>
<body>
	<div class="container h-100">
		<div class="row h-100 justify-content-center align-items-center">
			<div class="col-auto">
				<h1 class="display-4 text-center">Usuario no logueado</h1>
				<p class="lead text-center">Intente <a class="btn btn-primary" href="login.jsp" role="button">loguearse</a> nuevamente.</p>
			</div>
		</div>
	</div>
</body>
</html>