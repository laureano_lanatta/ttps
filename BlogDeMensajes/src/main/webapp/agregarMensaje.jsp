<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="blogDeMensajes.Message" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>Agregar mensajes</title>
</head>
<body>
	<%@ include file="navbar.jsp" %>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-4">
				<h1 class="display-4 text-center">Nuevo</h1>
				<form action="SaveMessage" method="post">
					<div class="form-group">
						<label for="messageBody">Mensaje:</label>
						<textarea class="form-control" id="messageBody" name="messageBody" rows="5"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Enviar</button>
					<a href="visualizarMensajes.jsp" class="btn btn-danger">Cancelar</a>
				</form>				
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>