<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="blogDeMensajes.Message" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>Salir</title>
</head>
<body>
	<div class="container h-100">
		<div class="row h-100 justify-content-center align-items-center">
			<div class="col-auto">
				<p class="lead">Está seguro que desea <strong>salir</strong>?</p>
				<a href="Logout" class="btn btn-success">Si</a>
				<a href="visualizarMensajes.jsp" class="btn btn-danger">No</a>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>