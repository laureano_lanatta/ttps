<nav class="navbar navbar-expand-md navbar-light bg-light">
	<a class="navbar-brand" href=#>Blog de Mensajes</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="index.jsp">Documentos<span class="sr-only">(current)</span></a>
			</li>
			<%
				//La misma chanchada que en Filtro...
				if (session.getAttribute("user") != null) {
			%>
					<li class="nav-item disabled">
						<a class="nav-link disabled" href=#>${user.name}</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="agregarMensaje.jsp">Escribir mensaje</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="visualizarMensajes.jsp">Ver mensajes</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="salir.jsp">Salir</a>
					</li>
			<%
				//Es la mejor forma de armar la barra en funcion de la sesion??
				//Si ademas hay varios perfiles de usuarios con diferentes opciones?? Mas if's??
				} else {
			%>
					<li class="nav-item">
						<a class="nav-link" href="login.jsp">Login</a>
					</li>
			<%
				}
			%>
		</ul>
	</div>
</nav>