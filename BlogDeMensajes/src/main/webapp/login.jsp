﻿<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>Login</title>
</head>
<body>
	<div class="container h-100">
		<div class="row h-100 justify-content-center align-items-center">
			<div class="col-auto">
				<form action="Login" method="post">
					<div class="form-group">
						<label for="userName">Usuario</label>
						<input type="text" class="form-control" id="userName" name="userName">
					</div>
					<div class="form-group">
						<label for="userPass">Contraseña</label>
						<input type="password" class="form-control" id="userPass" name="userPass">
					</div>
					<button type="submit" class="btn btn-primary">Ingresar</button>
					<a href="visualizarMensajes.jsp" class="btn btn-danger">Cancelar</a>
				</form>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>