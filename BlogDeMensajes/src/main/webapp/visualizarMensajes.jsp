<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="blogDeMensajes.Message" %>
<%@ page import="dao.FactoryDAO" %>
<%@ page import="dao.MessageDAO" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>Visualizar mensajes</title>
</head>
<body>
	<%@ include file="navbar.jsp" %>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-8">
				<h1 class="display-3 text-center">Mensajes</h1>
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">USUARIO</th>
							<th scope="col">MENSAJE</th>
						</tr>
					</thead>
					<tbody>
						<%
							MessageDAO messageDAO = FactoryDAO.getMessageDAO();
							List<Message> messages = messageDAO.getAll();
							for (Message message : messages) {
								String me = "";
								if (session.getAttribute("user") != null && message.getAuthor().equals(session.getAttribute("user")))
									me = "font-weight-bold";
						%>
								<tr>
									<td class="<%= me %>"><%= message.getAuthor().getName() %></td>
									<td><%= message.getBody() %></td>
								</tr>
						<%
							}
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>