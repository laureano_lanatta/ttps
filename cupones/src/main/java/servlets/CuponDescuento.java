package servlets;

import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
        urlPatterns = "/CuponDescuento",
        initParams = @WebInitParam(name = "fechaPromo", value = "15/12/2018")
)
public class CuponDescuento extends HttpServlet {
	private static final long serialVersionUID = 1L;	
	private String fecha;
	
	public void init() throws ServletException {
		fecha = this.getServletConfig().getInitParameter("fechaPromo");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();
		BufferedImage image = new BufferedImage(385, 550, BufferedImage.TYPE_INT_BGR);
		Graphics2D graphics = image.createGraphics();
		
		BufferedImage img = ImageIO.read(this.getServletContext().getResourceAsStream("/WEB-INF/classes/soloMoviePoster.jpg"));
		graphics.drawImage(img, 0, 0, null, null);
		
		graphics.setFont(new Font("TimesRoman", Font.BOLD, 20));
		graphics.setColor(Color.CYAN);
		graphics.drawString("CUPON VALIDO PARA EL DIA:", 50, 50);
		graphics.drawString(this.fecha, 130, 80);
		graphics.setColor(Color.YELLOW);
		graphics.drawString(request.getParameter("nombre") + " "
							+ request.getParameter("apellido"), 110, 140);
		graphics.drawString("DNI " + request.getParameter("dni"), 110, 170);
				
		ImageIO.write(image, "jpg", out);		
		out.close();
	}

}
