export class Credential {

    public userID: string;
    public userPWD: string;

    constructor(userID: string, userPWD: string) {
        this.userID = userID;
        this.userPWD = userPWD;
    }

}