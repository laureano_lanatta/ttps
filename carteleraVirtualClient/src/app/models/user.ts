export class User {

    public id: string;
	public user_id: string;
	public username: string;
	public profile: string;

    constructor(id: string, user_id: string, username: string, profile: string) {
        this.id = id;
        this.user_id = user_id;
        this.username = username;
        this.profile = profile;
    }

}