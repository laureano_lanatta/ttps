export class Billboard {

    public name: string;
    public tags: [];

    constructor(id: string, name: string) {
        this.name = name;
        this.tags = [];
    }

}