import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public menuOptions = [];
  public token;
  public username;
  public profile;

  constructor(
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.token = localStorage.getItem("token");
    if (this.token) {
      this.username = JSON.parse(localStorage.getItem("user")).username;
      this.profile = JSON.parse(localStorage.getItem("user")).profile;
      let options = {
        Administrador: [{ref: "/newBillboard", btn: "Nueva Cartelera"}, {ref: "/admin_2", btn: "Op Admin 2"}, {ref: "/admin_3", btn: "Op Admin 3"}],
        Estudiante: [{ref: "/est_1", btn: "Op Est 1"}, {ref: "/est_2", btn: "Op Est 2"}, {ref: "/est_3", btn: "Op Est 3"}],
        Profesor: [{ref: "/prof_1", btn: "Op Prof 1"}, {ref: "/prof_2", btn: "Op Prof 2"}, {ref: "/prof_3", btn: "Op Prof 3"}],
        Publicador: [{ref: "/pub_1", btn: "Op Pub 1"}, {ref: "/pub_2", btn: "Op Pub 2"}, {ref: "/pub_3", btn: "Op Pub 3"}]
      }
      this.menuOptions = options[this.profile];
    }
  }

  public logout() {
    this.authenticationService.logout();
  }

  
  


}
