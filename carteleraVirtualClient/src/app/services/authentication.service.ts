import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Credential } from '../models/credential';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private http: HttpClient
  ) { }

  public login(credentials: Credential) {
    const uri = 'http://localhost:8080/carteleraVirtual/auth';
    this.http.post(uri, credentials, {observe: 'response'})
      .subscribe(response => {
        let token = response.headers.get("Authorization").split(' ')[1];
        localStorage.setItem("token", token);
        localStorage.setItem("user", JSON.stringify(response.body));
        window.location.replace("/");
        alert("Bienvenido!");
      },
        error => {
          if (error.status == 401) {
            alert("Credenciales invalidas!");
          } else {
            alert("Ha ocurrido un error. Intente más tarde.");
          }
        }
      );
  }

  public logout() {
    const uri = 'http://localhost:8080/carteleraVirtual/deauth';
    const token = "Bearer " + localStorage.getItem("token");
    this.http.post(uri,
                  {},
                  {
                    headers: new HttpHeaders({
                      'Content-Type':  'application/json',
                      'Authorization': token,
                      'revoke': 'true'
                  })})
      .subscribe(response => {
        alert("Saliendo");
      },
      error => {
        console.log(error);
        alert("Ha ocurrido un error. Intente más tarde.");
      }
    );
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    //window.location.replace("/");
  }

}
