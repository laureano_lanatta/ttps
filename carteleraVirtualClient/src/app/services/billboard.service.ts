import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Billboard } from '../models/billboard';

@Injectable({
    providedIn: 'root'
})
export class BillboardService {

    constructor(
        private http: HttpClient
    ) { }

    public getTags() {
        const token = "Bearer " + localStorage.getItem("token");
        const uri = 'http://localhost:8080/carteleraVirtual/tags';
        this.http.get(uri,
            {headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': token
            }), observe: 'response'})
            .subscribe(response => {
                return response;
            },
            error => {
              alert("Ha ocurrido un error. Intente más tarde.");
            }
          );
    }

    public addTags(billboard_id: string, tags: []) {
        const token = "Bearer " + localStorage.getItem("token");
        const tag_uri = 'http://localhost:8080/carteleraVirtual/billboards/'
                        + billboard_id
                        + '/tags/';
        for (let tag of tags) {
            this.http.post(tag_uri + tag,
                           {headers: new HttpHeaders({
                                'Content-Type':  'application/json',
                                'Authorization': token
                            }), observe: 'response'})
            .subscribe(response => {
                console.log(response);
            },
            error => {
              alert("Ha ocurrido un error. Intente más tarde.");
            }
          );
        }
    }

    public create(billboard: Billboard) {
        const token = "Bearer " + localStorage.getItem("token");
        const billboard_uri = 'http://localhost:8080/carteleraVirtual/billboards';
        this.http.post(billboard_uri,
                       billboard,
                       {headers: new HttpHeaders({
                            'Content-Type':  'application/json',
                            'Authorization': token
                        }), observe: 'response'})
          .subscribe(response => {
            this.addTags(response.body["id"], billboard.tags);
          },
          error => {
            alert("Ha ocurrido un error. Intente más tarde.");
          }
        );
    }

}