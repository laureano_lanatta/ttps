import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Credential } from '../models/credential';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  model = new Credential("root", "admin");

  submitted = false;
  onSubmit() { 
    this.submitted = true;
    this.authenticationService.login(this.model);
  }

  get diagnostic() { return JSON.stringify(this.model); }

  ngOnInit() {
  }

}
