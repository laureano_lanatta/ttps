import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBillboardComponent } from './new-billboard.component';

describe('NewBillboardComponent', () => {
  let component: NewBillboardComponent;
  let fixture: ComponentFixture<NewBillboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBillboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBillboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
