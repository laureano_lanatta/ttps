package misServlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Premio extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private int visitCount;

	public void init() throws ServletException {
		this.visitCount = 0;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.visitCount++;
		String visitor = request.getParameter("nombre");
		String count = String.valueOf(this.visitCount);
		String msg = this.getServletConfig().getInitParameter("msg")
						.replace("@", visitor)
						.replace("#", count);

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html><body>");
		out.println("<p>" + msg + "</p>");
		out.print("</body></html>");
		out.close();
	}

}
