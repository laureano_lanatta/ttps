package carteleraVirtual.controllers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import carteleraVirtual.model.Comment;
import carteleraVirtual.model.dto.CommentDTO;
import carteleraVirtual.services.CommentService;

@RestController
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	@GetMapping(path = "/comments")
	public List<CommentDTO> getAll() {
		return this.commentService.getAll()
								  .stream()
								  .map(comment -> new CommentDTO(comment))
								  .collect(Collectors.toList());
	}
	
	@GetMapping(path = "/comments/{id}")
	public CommentDTO get(@PathVariable("id") long id) {
		CommentDTO commentDTO = null;
		Comment comment = this.commentService.get(id);
		if (comment != null)
			commentDTO = new CommentDTO(comment);
		return commentDTO;
	}
	
	@PostMapping(path = "/comments")
	public CommentDTO create(@RequestBody Comment comment) {
		return new CommentDTO(this.commentService.create(comment));
	}
	
}
