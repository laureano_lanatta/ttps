package carteleraVirtual.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import carteleraVirtual.model.User;
import carteleraVirtual.model.dto.UserDTO;
import carteleraVirtual.services.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping(path = "/users")
	public List<UserDTO> getAll() {
		return this.userService.getAll();
	}
	
	@GetMapping(path = "/users/{id}")
	public UserDTO get(@PathVariable("id") long id) {
		return this.userService.get(id);
	}
	
	@PostMapping(path = "/users")
	public UserDTO create(@RequestBody User user) {
		return this.userService.create(user);
	}
	
}
