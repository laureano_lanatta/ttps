package carteleraVirtual.controllers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import carteleraVirtual.model.Tag;
import carteleraVirtual.model.dto.TagDTO;
import carteleraVirtual.services.TagService;

@RestController
public class TagController {

	@Autowired
	private TagService tagService;
	
	@GetMapping(path = "/tags")
	public List<TagDTO> getAll() {
		return this.tagService.getAll();
	}
	
	@GetMapping(path = "/tags/{id}")
	public TagDTO get(@PathVariable("id") long id) {
		return this.tagService.get(id);
	}

	@PostMapping(path = "/tags")
	public TagDTO create(@RequestBody Tag tag) {
		return this.tagService.create(tag);
	}
	
}
