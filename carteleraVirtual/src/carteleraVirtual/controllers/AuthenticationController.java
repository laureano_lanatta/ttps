package carteleraVirtual.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import carteleraVirtual.model.UserCredential;
import carteleraVirtual.model.dto.UserDTO;
import carteleraVirtual.services.AuthenticationService;
import carteleraVirtual.services.UserService;
import io.jsonwebtoken.Jwts;
import carteleraVirtual.config.Environment;
import java.util.Date;
import java.util.UUID;

@RestController
public class AuthenticationController {

	@Autowired
	private AuthenticationService authenticationService;
	
	@Autowired
	private UserService userService;
	
	@PostMapping(path = "/auth")
	public ResponseEntity<UserDTO> authenticate(@RequestBody UserCredential userCredential) {
		if (this.authenticationService.exists(userCredential.getUserID(), userCredential.getUserPWD())) {
			Date now = new Date();
			long sessionTimeInMilliseconds = 86400000; //24hs
			String jws = Jwts.builder()
							 .setSubject(userCredential.getUserID())
							 .setIssuedAt(now)
							 .setExpiration(new Date(now.getTime() + sessionTimeInMilliseconds))
							 .setId(UUID.randomUUID().toString())
							 .signWith(Environment.KEY)
							 .compact();
			UserDTO userDTO = this.userService.getByUserID(userCredential.getUserID());
			return ResponseEntity.ok()
								 .header("Authorization", "Bearer " + jws)
								 .body(userDTO);
		} else {
			return new ResponseEntity<UserDTO>(HttpStatus.UNAUTHORIZED);
		}
	}
	
	@PostMapping(path = "/deauth")
	public ResponseEntity deauthenticate() {
		return new ResponseEntity(HttpStatus.OK);
	}
	
}
