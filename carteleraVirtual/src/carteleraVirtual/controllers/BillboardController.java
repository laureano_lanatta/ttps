package carteleraVirtual.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import carteleraVirtual.annotations.AuthenticationRequired;
import carteleraVirtual.config.Environment;
import carteleraVirtual.model.Billboard;
import carteleraVirtual.model.Tag;
import carteleraVirtual.model.dto.BillboardDTO;
import carteleraVirtual.services.BillboardService;
import carteleraVirtual.services.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

@RestController
public class BillboardController {

	@Autowired
	private BillboardService billboardService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping(path = "/billboards")
	public List<BillboardDTO> getAll() {
		return this.billboardService.getAll();
	}
	
	@GetMapping(path = "/billboards/{id}")
	//@AuthenticationRequired
	public BillboardDTO get(@PathVariable("id") long id) {
		return this.billboardService.get(id);
	}
	
	@PostMapping(path = "/billboards")
	//@AuthenticationRequired
	public ResponseEntity<BillboardDTO> create(@RequestBody Billboard billboard,
							   @RequestHeader("Authorization") String authorization) {
		String userID = Jwts.parser()
							.setSigningKey(Environment.KEY)
							.parseClaimsJws(authorization.replace("Bearer ", ""))
							.getBody()
							.getSubject();
		String profile = this.userService.getByUserID(userID).getProfile();
		if (profile.equals("Administrador"))
			return ResponseEntity.ok(this.billboardService.create(billboard));
		else
			return new ResponseEntity<BillboardDTO>(HttpStatus.FORBIDDEN);
		
	}
	
	@PostMapping(path = "/billboards/{billboard_id}/tags/{tag_id}")
	//@AuthenticationRequired
	public BillboardDTO addTag(@PathVariable("billboard_id") long billboardID,
							   @PathVariable("tag_id") long tagID) {
		return this.billboardService.addTag(billboardID, tagID);
	}
	
}
