package carteleraVirtual.controllers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import carteleraVirtual.model.Post;
import carteleraVirtual.model.dto.PostDTO;
import carteleraVirtual.services.PostService;

@RestController
public class PostController {

	@Autowired
	private PostService postService;
	
	@GetMapping(path = "/posts")
	public List<PostDTO> getAll() {
		return this.postService.getAll()
							   .stream()
							   .map(post -> new PostDTO(post))
							   .collect(Collectors.toList());
	}
	
	@GetMapping(path = "/posts/{id}")
	public PostDTO get(@PathVariable("id") long id) {
		PostDTO postDTO = null;
		Post post = this.postService.get(id);
		if (post != null)
			postDTO = new PostDTO(post);
		return postDTO;
	}

	@PostMapping(path = "/post")
	public PostDTO create(@RequestBody Post post) {
		return new PostDTO(this.postService.create(post));
	}
	
}
