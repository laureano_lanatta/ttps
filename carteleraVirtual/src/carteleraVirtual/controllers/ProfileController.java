package carteleraVirtual.controllers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import carteleraVirtual.model.Profile;
import carteleraVirtual.model.dto.ProfileDTO;
import carteleraVirtual.services.ProfileService;

@RestController
public class ProfileController {

	@Autowired
	private ProfileService profileService;
	
	@GetMapping(path = "/profiles")
	public List<ProfileDTO> getAll() {
		return this.profileService.getAll();
	}
	
	@GetMapping(path = "/profiles/{id}")
	public ProfileDTO get(@PathVariable("id") long id) {
		return this.profileService.get(id);
	}
	
	@PostMapping(path = "/profiles")
	public ProfileDTO create(@RequestBody Profile profile) {
		return this.profileService.create(profile);
	}
	
}
