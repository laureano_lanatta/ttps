package carteleraVirtual.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;
import carteleraVirtual.services.AuthenticationService;
import carteleraVirtual.model.UserCredential;

@RestController
public class UserCredentialController {

	@Autowired
	private AuthenticationService authenticationService;
	
	@GetMapping(path = "/credentials")
	public List<UserCredential> getAllCredentials() {
		return this.authenticationService.getAllCredentials();
	}
	
	@GetMapping(path = "/credentials/{id}/{pass}")
	public UserCredential getByIdAndPass(@PathVariable("id") String id, @PathVariable("pass") String pwd) {
		return this.authenticationService.getByIdAndPass(id, pwd);
	}
	
	@PostMapping(path = "/credentials")
	public UserCredential createUserCredential(@RequestBody UserCredential userCredential) {
		return this.authenticationService.createUserCredential(userCredential);
	}
	
	@PutMapping(path = "/credentials")
	public UserCredential updateUserCredential(@RequestBody UserCredential userCredential) {
		return this.authenticationService.updateUserCredential(userCredential);
	}
	
	@DeleteMapping(path = "/credentials")
	public UserCredential deleteUserCredential(@RequestBody UserCredential userCredential) {
		return this.authenticationService.deleteUserCredential(userCredential);
	}
	
}
