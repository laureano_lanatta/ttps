package carteleraVirtual.listeners;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import carteleraVirtual.model.Billboard;
import carteleraVirtual.model.Profile;
import carteleraVirtual.model.Tag;
import carteleraVirtual.model.User;
import carteleraVirtual.model.UserCredential;
import carteleraVirtual.model.dao.ProfileDAO;
import carteleraVirtual.services.AuthenticationService;
import carteleraVirtual.services.BillboardService;
import carteleraVirtual.services.ProfileService;
import carteleraVirtual.services.TagService;
import carteleraVirtual.services.UserService;

@Component
@Transactional
public class InitData {

	@Autowired
	private AuthenticationService authenticationService;
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private ProfileDAO profileDAO;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TagService tagservice;
	
	@Autowired
	private BillboardService billboardService;
	
	@PostConstruct
	private void inititialize() {
		this.profileService.create(new Profile("Administrador"));
		this.profileService.create(new Profile("Estudiante"));
		this.profileService.create(new Profile("Profesor"));
		this.profileService.create(new Profile("Publicador"));
		this.authenticationService.createUserCredential(new UserCredential("root", "admin"));
		this.authenticationService.createUserCredential(new UserCredential("j_perez", "123"));
		this.authenticationService.createUserCredential(new UserCredential("m_gomez", "456"));
		this.userService.create(new User("root", "God", this.profileDAO.getByName("Administrador")));
		this.userService.create(new User("j_perez", "Juan Perez", this.profileDAO.getByName("Estudiante")));
		this.userService.create(new User("m_gomez", "Maria Gomez", this.profileDAO.getByName("Estudiante")));
		this.tagservice.create(new Tag("Academico"));
		this.tagservice.create(new Tag("Profesional"));
		this.tagservice.create(new Tag("Eventos"));
		this.tagservice.create(new Tag("Seguridad"));
		this.tagservice.create(new Tag("Robotica"));
		this.tagservice.create(new Tag("Extracurricular"));
		this.billboardService.create(new Billboard("Primer año"));
		this.billboardService.create(new Billboard("Postgrado"));
		this.billboardService.create(new Billboard("Centro de estudiantes"));
		this.billboardService.create(new Billboard("Oferta laboral"));
	}

}
