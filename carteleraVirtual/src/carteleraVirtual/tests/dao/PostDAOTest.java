package carteleraVirtual.tests.dao;
//package dao;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import carteleraVirtual.model.Billboard;
//import carteleraVirtual.model.EMF;
//import carteleraVirtual.model.Post;
//import carteleraVirtual.model.dao.FactoryDAO;
//import carteleraVirtual.model.dao.PostDAO;
//
//class PostDAOTest {
//
//	@BeforeAll
//	static void setUpBeforeClass() throws Exception {
//		EMF.init();
//	}
//
//	@AfterAll
//	static void tearDownAfterClass() throws Exception {
//		EMF.close();
//	}
//
//	@Test
//	void getByTitle_WithCorrectData() {
//		Post post = new Post();
//		String title = "t1";
//		post.setTitle(title);
//		PostDAO postDAO = FactoryDAO.getPostDAO();
//		postDAO.save(post);
//		assertEquals(post, postDAO.getByTitle(title));
//	}
//	
//	@Test
//	void getByTitle_WithIncorrectData() {
//		Post post = new Post();
//		String title = "t2";
//		post.setTitle(title);
//		PostDAO postDAO = FactoryDAO.getPostDAO();
//		postDAO.save(post);
//		assertNull(postDAO.getByTitle("FAIL"));
//	}
//	
//}
