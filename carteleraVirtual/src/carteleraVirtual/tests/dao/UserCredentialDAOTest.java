package carteleraVirtual.tests.dao;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import carteleraVirtual.model.UserCredential;
import carteleraVirtual.model.dao.UserCredentialDAO;
import java.util.ArrayList;
import java.util.List;

class UserCredentialDAOTest {

	@Autowired
	private UserCredentialDAO userCredentialDAO;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		//EMF.init();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		//EMF.close();
	}
	
	@BeforeEach
	void setUp() throws Exception {
		//Esto es CARISIMO!
		//EMF.init();
	}

	@AfterEach
	void tearDown() throws Exception {
		//Esto es CARISIMO!
		//EMF.close();
	}
	
	@Test
	void getByIdAndPwd_WithCorrectData() {
		String userID = "a";
		String userPWD = "1";
		UserCredential credential = new UserCredential(userID, userPWD);
		this.userCredentialDAO.save(credential);
		UserCredential userCredential = this.userCredentialDAO.getByIdAndPwd(userID, userPWD);
		assertEquals(credential, userCredential);
	}
	
	@Test
	void getByIdAndPwd_WithIncorrectData() {
		String userID = "Fail";
		String userPWD = "Fail";
		UserCredential userCredential = this.userCredentialDAO.getByIdAndPwd(userID, userPWD);
		assertNull(userCredential);
	}
	
	@Test
	void get_WithCorrectId() {
		String userID = "b";
		String userPWD = "2";
		UserCredential credential = new UserCredential(userID, userPWD);
		Long id = this.userCredentialDAO.save(credential).getId();
		UserCredential resultCredential = this.userCredentialDAO.get(id);
		assertEquals(credential, resultCredential);
	}
	
	@Test
	void get_WithIncorrectId() {
		String userID = "c";
		String userPWD = "3";
		UserCredential credential = new UserCredential(userID, userPWD);
		Long id = this.userCredentialDAO.save(credential).getId() - 1;
		UserCredential resultCredential = this.userCredentialDAO.get(id);
		assertNotEquals(credential, resultCredential);
	}
	
	@Test
	void get_WithNoneExistentId() {
		Long id = 0L;
		String userID = "d";
		String userPWD = "4";
		UserCredential credential = new UserCredential(userID, userPWD);
		this.userCredentialDAO.save(credential);
		UserCredential resultCredential = this.userCredentialDAO.get(id);
		assertNull(resultCredential);
	}
	
	@Test
	void exists_WithExistentId() {
		String userID = "e";
		String userPWD = "5";
		UserCredential credential = new UserCredential(userID, userPWD);
		Long id = this.userCredentialDAO.save(credential).getId();
		assertTrue(this.userCredentialDAO.exists(id));
	}
	
	@Test
	void exists_WithNoneExistentId() {
		Long id = 0L;
		String userID = "f";
		String userPWD = "6";
		UserCredential credential = new UserCredential(userID, userPWD);
		this.userCredentialDAO.save(credential);
		assertFalse(this.userCredentialDAO.exists(id));
	}
	
	@Test
	void save_Entity() {
		String userID = "g";
		String userPWD = "7";
		UserCredential credential = new UserCredential(userID, userPWD);
		this.userCredentialDAO.save(credential);
		UserCredential userCredential = this.userCredentialDAO.getByIdAndPwd(userID, userPWD);
		assertEquals(credential, userCredential);
	}	

	@Test
	void update() {
		String userID = "h";
		String userPWD = "8";
		String newUserPWD = "8888";
		UserCredential credential = new UserCredential(userID, userPWD);
		Long id = this.userCredentialDAO.save(credential).getId();
		credential.setUserPWD(newUserPWD);
		this.userCredentialDAO.update(credential);	
		UserCredential updatedCredential = this.userCredentialDAO.get(id);
		assertEquals(newUserPWD, updatedCredential.getUserPWD());
	}
	
	@Test
	void delete_WithEntity() {
		String userID = "i";
		String userPWD = "9";
		UserCredential credential = new UserCredential(userID, userPWD);
		Long id = this.userCredentialDAO.save(credential).getId();
		this.userCredentialDAO.delete(credential);	
		UserCredential removedCredential = this.userCredentialDAO.get(id);
		assertNull(removedCredential);
	}
	
	@Test
	void delete_WithId() {
		String userID = "j";
		String userPWD = "10";
		UserCredential credential = new UserCredential(userID, userPWD);
		Long id = this.userCredentialDAO.save(credential).getId();
		this.userCredentialDAO.delete(id);	
		UserCredential removedCredential = this.userCredentialDAO.get(id);
		assertNull(removedCredential);
	}
	
	@Test
	void getAll() {
		List<UserCredential> credentials = this.userCredentialDAO.getAll();
		for (UserCredential credential : credentials)
			this.userCredentialDAO.delete(credential);
		credentials = new ArrayList<UserCredential>();
		credentials.add(new UserCredential("k", "11"));
		credentials.add(new UserCredential("l", "12"));
		credentials.add(new UserCredential("m", "13"));
		for (UserCredential credential : credentials)
			this.userCredentialDAO.save(credential);
		assertArrayEquals(credentials.toArray(), this.userCredentialDAO.getAll().toArray());
	}
	
	@Test void z() {
		UserCredential credential = new UserCredential("zName", "zPass");
		this.userCredentialDAO.save(credential);
		UserCredential cred = this.userCredentialDAO.getByIdAndPwd("zName", "zPass");
		cred.setUserPWD("zPass2");
		this.userCredentialDAO.update(cred);
		this.userCredentialDAO.save(cred);
		UserCredential cred2 = this.userCredentialDAO.getByIdAndPwd("zName", "zPass2");
		System.out.println(cred2.getUserID() + ", " + cred2.getUserPWD());
	}
	
}
