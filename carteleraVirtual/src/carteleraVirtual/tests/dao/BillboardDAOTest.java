package carteleraVirtual.tests.dao;
//package dao;
//
//import static org.junit.jupiter.api.Assertions.*;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import carteleraVirtual.model.Billboard;
//import carteleraVirtual.model.EMF;
//import carteleraVirtual.model.dao.BillboardDAO;
//import carteleraVirtual.model.dao.FactoryDAO;
//
//class BillboardDAOTest {
//
//	@BeforeAll
//	static void setUpBeforeClass() throws Exception {
//		EMF.init();
//	}
//
//	@AfterAll
//	static void tearDownAfterClass() throws Exception {
//		EMF.close();
//	}
//	
//	@Test
//	void getByName_WithCorrectData() {
//		Billboard billboard = new Billboard();
//		String name = "b1";
//		billboard.setName(name);
//		BillboardDAO billboardDAO = FactoryDAO.getBillboardDAO();
//		billboardDAO.save(billboard);
//		assertEquals(billboard, billboardDAO.getByName(name));
//	}
//	
//	@Test
//	void getByName_WithIncorrectData() {
//		BillboardDAO billboardDAO = FactoryDAO.getBillboardDAO();
//		assertNull(billboardDAO.getByName("FAIL"));
//	}
//	
//}
