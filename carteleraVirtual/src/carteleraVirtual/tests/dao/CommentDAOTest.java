package carteleraVirtual.tests.dao;
//package dao;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import carteleraVirtual.model.Comment;
//import carteleraVirtual.model.EMF;
//import carteleraVirtual.model.Post;
//import carteleraVirtual.model.Student;
//import carteleraVirtual.model.User;
//import carteleraVirtual.model.dao.CommentDAO;
//import carteleraVirtual.model.dao.FactoryDAO;
//import carteleraVirtual.model.dao.PostDAO;
//import carteleraVirtual.model.dao.UserDAO;
//
//class CommentDAOTest {
//
//	@BeforeAll
//	static void setUpBeforeClass() throws Exception {
//		EMF.init();
//	}
//
//	@AfterAll
//	static void tearDownAfterClass() throws Exception {
//		EMF.close();
//	}
//	
//	@Test
//	void getByAuthor() {
//		CommentDAO commentDAO = FactoryDAO.getCommentDAO();
//		UserDAO userDAO = FactoryDAO.getUserDAO();
//		PostDAO postDAO = FactoryDAO.getPostDAO();
//		User cu1 = new Student("cu1", "cu1");
//		User cu2 = new Student("cu2", "cu2");
//		userDAO.save(cu1);
//		userDAO.save(cu2);
//		Post cp1 = new Post();
//		Post cp2 = new Post();
//		postDAO.save(cp1);
//		postDAO.save(cp2);
//		Comment c1 = new Comment(cu1, cp1, "cu1-cp1-1");
//		Comment c2 = new Comment(cu1, cp2, "cu1-cp2-1");
//		Comment c3 = new Comment(cu1, cp1, "cu1-cp1-2");
//		Comment c4 = new Comment(cu2, cp1, "cu1-cp1-1");
//		Comment c5 = new Comment(cu2, cp2, "cu1-cp2-1");
//		commentDAO.save(c1);
//		commentDAO.save(c2);
//		commentDAO.save(c3);
//		commentDAO.save(c4);
//		commentDAO.save(c5);
//		List<Comment> l = new ArrayList<Comment>();
//		l.add(c1);
//		l.add(c2);
//		l.add(c3);
//		cu1.setComments(l);
//		userDAO.save(cu1);
//		cu1 = userDAO.getByUserID("cu1");
//		Comment[] expectedComments = new Comment[] {c1, c2, c3};
//		assertArrayEquals(expectedComments, commentDAO.getByAuthor(cu1).toArray());
//		for (Comment c : cu1.getComments())
//			System.out.println(c.getBody());
//		//assertArrayEquals(cu1.getComments().toArray(), commentDAO.getByAuthor(cu1).toArray());
//	}
//	/*
//	@Test
//	void getByPost() {
//		CommentDAO commentDAO = FactoryDAO.getCommentDAO();
//		UserDAO userDAO = FactoryDAO.getUserDAO();
//		PostDAO postDAO = FactoryDAO.getPostDAO();
//		User cu1 = new Student("cu1", "cu1");
//		User cu2 = new Student("cu2", "cu2");
//		userDAO.save(cu1);
//		userDAO.save(cu2);
//		Post cp1 = new Post();
//		Post cp2 = new Post();
//		postDAO.save(cp1);
//		postDAO.save(cp2);
//		Comment c1 = new Comment(cu1, cp1, "cu1-cp1-1");
//		Comment c2 = new Comment(cu1, cp2, "cu1-cp2-1");
//		Comment c3 = new Comment(cu1, cp1, "cu1-cp1-2");
//		Comment c4 = new Comment(cu2, cp1, "cu1-cp1-1");
//		Comment c5 = new Comment(cu2, cp2, "cu1-cp2-1");
//		commentDAO.save(c1);
//		commentDAO.save(c2);
//		commentDAO.save(c3);
//		commentDAO.save(c4);
//		commentDAO.save(c5);
//		Comment[] expectedComments = new Comment[] {c1, c3, c4};
//		assertArrayEquals(expectedComments, commentDAO.getByPost(cp1).toArray());
//	}
//*/
//}
