package carteleraVirtual.tests.dao;
//package dao;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import carteleraVirtual.model.EMF;
//import carteleraVirtual.model.Tag;
//import carteleraVirtual.model.dao.FactoryDAO;
//import carteleraVirtual.model.dao.TagDAO;
//
//class TagDAOTest {
//
//	@BeforeAll
//	static void setUpBeforeClass() throws Exception {
//		EMF.init();
//	}
//
//	@AfterAll
//	static void tearDownAfterClass() throws Exception {
//		EMF.close();
//	}
//
//	@Test
//	void getByName_WithCorrectData() {
//		Tag t1 = new Tag();
//		String name = "t1";
//		t1.setName(name);
//		TagDAO tagDAO = FactoryDAO.getTagDAO();
//		tagDAO.save(t1);
//		assertEquals(t1, tagDAO.getByName(name));
//	}
//	
//	@Test
//	void getByName_WithIncorrectData() {
//		TagDAO tagDAO = FactoryDAO.getTagDAO();
//		assertNull(tagDAO.getByName("FAIL"));
//	}
//	
//}
