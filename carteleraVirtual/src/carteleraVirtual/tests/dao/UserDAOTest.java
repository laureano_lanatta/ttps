package carteleraVirtual.tests.dao;
//package dao;
//
//import static org.junit.jupiter.api.Assertions.*;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import carteleraVirtual.model.Admin;
//import carteleraVirtual.model.EMF;
//import carteleraVirtual.model.Publisher;
//import carteleraVirtual.model.Student;
//import carteleraVirtual.model.Teacher;
//import carteleraVirtual.model.User;
//import carteleraVirtual.model.dao.FactoryDAO;
//import carteleraVirtual.model.dao.UserDAO;
//
//class UserDAOTest {
//
//	@BeforeAll
//	static void setUpBeforeClass() throws Exception {
//		EMF.init();
//	}
//
//	@AfterAll
//	static void tearDownAfterClass() throws Exception {
//		EMF.close();
//	}
//	
//	@Test
//	void getByUserID_WithCorrectID() {
//		User user = new Admin();
//		user.setUsername("a");
//		user.setUserID("1");
//		UserDAO userDAO = FactoryDAO.getUserDAO();
//		String userID = userDAO.save(user).getUserID();
//		User newUser = userDAO.getByUserID(userID);
//		assertEquals(user, newUser);
//	}
//	
//	@Test
//	void getByUserID_WithInorrectID() {
//		User user = new Student();
//		user.setUsername("b");
//		user.setUserID("2");
//		UserDAO userDAO = FactoryDAO.getUserDAO();
//		String userID = userDAO.save(user).getUserID() + "FAIL";
//		User newUser = userDAO.getByUserID(userID);
//		assertNotEquals(user, newUser);
//	}
//	
//	@Test
//	void getByUsername_WithCorrectUsername() {
//		User user = new Teacher();
//		user.setUsername("c");
//		user.setUserID("3");
//		UserDAO userDAO = FactoryDAO.getUserDAO();
//		String username = userDAO.save(user).getUsername();
//		User newUser = userDAO.getByUsername(username);
//		assertEquals(user, newUser);
//	}
//
//	@Test
//	void getByUsername_WithIncorrectUsername() {
//		User user = new Publisher();
//		user.setUsername("d");
//		user.setUserID("4");
//		UserDAO userDAO = FactoryDAO.getUserDAO();
//		String username = userDAO.save(user).getUsername() + "FAIL";
//		User newUser = userDAO.getByUsername(username);
//		assertNotEquals(user, newUser);
//	}
//
//}
