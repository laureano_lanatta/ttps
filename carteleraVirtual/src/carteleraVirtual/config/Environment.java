package carteleraVirtual.config;

import javax.crypto.SecretKey;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class Environment {

	public static final SecretKey KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);	
	
}
