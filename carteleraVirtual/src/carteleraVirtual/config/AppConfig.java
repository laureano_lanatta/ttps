package carteleraVirtual.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import carteleraVirtual.interceptors.AuthenticationInterceptor;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "carteleraVirtual")
public class AppConfig implements WebMvcConfigurer {
	/*
	@Autowired
	AuthenticationInterceptor authenticationInterceptor;
	*/
	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new MappingJackson2HttpMessageConverter());
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/*")
				.allowCredentials(true)
				.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
				.allowedOrigins("http://localhost:4200")
				.allowedHeaders("*")
				.exposedHeaders("Access-Control-Allow-Origin", "Authorization");
	}
	/*
	@Override
    public void addInterceptors (InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor);
    }
	*/
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new Hibernate5Module());
		messageConverter.setObjectMapper(mapper);
		converters.add(messageConverter);
	}
	
}
