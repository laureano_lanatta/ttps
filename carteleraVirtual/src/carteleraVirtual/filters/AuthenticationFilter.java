package carteleraVirtual.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import carteleraVirtual.config.Environment;
import carteleraVirtual.model.Blacklist;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {

	private FilterConfig filterConfig;
	private Blacklist blacklist;
	
    public AuthenticationFilter() {
        
    }

    public void init(FilterConfig fConfig) throws ServletException {
		this.filterConfig = fConfig;
		this.blacklist = new Blacklist();
	}

    
	public void destroy() {
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		/*
		if(HttpMethod.OPTIONS.name().equals(req.getMethod())) {
			chain.doFilter(request, response);
			return;
		}
		*/
        if ("/carteleraVirtual/auth".equals(req.getRequestURI()) || HttpMethod.OPTIONS.matches(req.getMethod())) {
        	chain.doFilter(request, response);
            return;
        }
        
        try {
			String authorization = req.getHeader("authorization");
			String jws = authorization.replace("Bearer ", "");
			Jws<Claims> token = Jwts.parser()
					 				.setSigningKey(Environment.KEY)
					 				.parseClaimsJws(jws);
			if (this.blacklist.contains(token))
				throw new Exception("Token invalidated");
			if (req.getHeader("revoke") != null)
				this.blacklist.add(token);
			chain.doFilter(request, response);
		} catch (Exception e) {
			res.setStatus(401);
		}
	}

	
}
