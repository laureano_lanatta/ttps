package carteleraVirtual.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebFilter("/*")
public class CorsFilter implements Filter {

	private FilterConfig filterConfig;
	
    public CorsFilter() {
        
    }
    
    public void init(FilterConfig fConfig) throws ServletException {
		this.filterConfig = fConfig;
	}

	public void destroy() {
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("En el filtro!");
		HttpServletRequest req = (HttpServletRequest)request;                                   
        HttpServletResponse res = (HttpServletResponse)response;		
		if(req.getHeader("Origin") != null){
            res.addHeader("Access-Control-Allow-Origin", "*");
            res.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, Authorization");
        }

        if("OPTIONS".equals(req.getMethod())){
            res.addHeader("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
            res.addHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Origin, Content-Type, Authorization");
        }    
		chain.doFilter(request, response);
	}

}
