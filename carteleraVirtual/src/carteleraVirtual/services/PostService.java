package carteleraVirtual.services;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import carteleraVirtual.model.Post;
import carteleraVirtual.model.dao.PostDAO;

@Service
@Transactional
public class PostService {

	@Autowired
	private PostDAO postDAO;
	
	public List<Post> getAll() {
		return this.postDAO.getAll();
	}
	
	public Post get(long id) {
		return this.postDAO.get(id);
	}
	
	public Post create(Post post) {
		return this.postDAO.save(post);
	}
	
}
