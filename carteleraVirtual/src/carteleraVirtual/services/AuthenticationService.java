package carteleraVirtual.services;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carteleraVirtual.model.UserCredential;
import carteleraVirtual.model.dao.UserCredentialDAO;

@Service
@Transactional
public class AuthenticationService {

	@Autowired
	private UserCredentialDAO userCredentialDAO;

	public List<UserCredential> getAllCredentials() {
		return this.userCredentialDAO.getAll();
	}
	
	public boolean exists(String userID, String userPWD) {
		return this.getByIdAndPass(userID, userPWD) != null;
	}
	
	public UserCredential getByIdAndPass(String id, String pwd) {
		return this.userCredentialDAO.getByIdAndPwd(id, pwd);
	}
	
	public UserCredential createUserCredential(UserCredential userCredential) {
		return this.userCredentialDAO.save(userCredential);
	}
	
	public UserCredential updateUserCredential(UserCredential userCredential) {
		return this.userCredentialDAO.update(userCredential);
	}
	
	public UserCredential deleteUserCredential(UserCredential userCredential) {
		return this.userCredentialDAO.delete(userCredential);
	}
	
}
