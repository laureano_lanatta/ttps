package carteleraVirtual.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carteleraVirtual.model.Billboard;
import carteleraVirtual.model.Tag;
import carteleraVirtual.model.dao.BillboardDAO;
import carteleraVirtual.model.dao.TagDAO;
import carteleraVirtual.model.dto.BillboardDTO;

@Service
@Transactional
public class BillboardService {

	@Autowired
	private BillboardDAO billboardDAO;
	
	@Autowired
	private TagDAO tagDAO;
	
	public List<BillboardDTO> getAll() {
		return this.billboardDAO.getAll()
								.stream()
								.map(billboard -> new BillboardDTO(billboard))
								.collect(Collectors.toList());
	}
	
	public BillboardDTO get(long id) {
		BillboardDTO billboardDTO = null;
		Billboard billboard = this.billboardDAO.get(id);
		if (billboard != null)
			billboardDTO = new BillboardDTO(billboard);
		return billboardDTO;
	}
	
	public BillboardDTO create(Billboard billboard) {
		return new BillboardDTO(this.billboardDAO.save(billboard));
	}
	
	public BillboardDTO addTag(long billboardID, long tagID) {
		Billboard billboard = this.billboardDAO.get(billboardID);
		Tag tag = this.tagDAO.get(tagID);
		billboard.addTag(tag);
		tag.addBillboard(billboard);
		return new BillboardDTO(billboard);
	}
	
}
