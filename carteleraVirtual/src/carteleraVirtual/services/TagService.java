package carteleraVirtual.services;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carteleraVirtual.model.Tag;
import carteleraVirtual.model.dao.TagDAO;
import carteleraVirtual.model.dto.TagDTO;

@Service
@Transactional
public class TagService {

	@Autowired
	private TagDAO tagDAO;
	
	public List<TagDTO> getAll() {
		return this.tagDAO.getAll()
						  .stream()
						  .map(tag -> new TagDTO(tag))
						  .collect(Collectors.toList());
	}
	
	public TagDTO get(long id) {
		TagDTO tagDTO = null;
		Tag tag = this.tagDAO.get(id);
		if (tag != null)
			tagDTO = new TagDTO(tag);
		return tagDTO;
	}
	
	public TagDTO create(Tag tag) {
		return new TagDTO(this.tagDAO.save(tag));
	}
	
}
