package carteleraVirtual.services;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carteleraVirtual.model.Profile;
import carteleraVirtual.model.dao.ProfileDAO;
import carteleraVirtual.model.dto.ProfileDTO;

@Service
@Transactional
public class ProfileService {

	@Autowired
	private ProfileDAO profileDAO;
	
	public List<ProfileDTO> getAll() {
		return this.profileDAO.getAll()
							  .stream()
							  .map(profile -> new ProfileDTO(profile))
							  .collect(Collectors.toList());
	}
	
	public ProfileDTO get(long id) {
		ProfileDTO profileDTO = null;
		Profile profile = this.profileDAO.get(id);
		if (profile != null)
			profileDTO = new ProfileDTO(profile);
		return profileDTO;
	}
	
	public ProfileDTO create(Profile profile) {
		return new ProfileDTO(this.profileDAO.save(profile));
	}
	
}
