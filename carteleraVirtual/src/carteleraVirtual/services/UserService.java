package carteleraVirtual.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carteleraVirtual.model.User;
import carteleraVirtual.model.dao.UserDAO;
import carteleraVirtual.model.dto.UserDTO;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserDAO userDAO;

	public List<UserDTO> getAll() {
		return this.userDAO.getAll()
						   .stream()
						   .map(user -> new UserDTO(user))
						   .collect(Collectors.toList());
	}

	public UserDTO get(long id) {
		UserDTO userDTO = null;
		User user = this.userDAO.get(id);
		if (user != null)
			userDTO = new UserDTO(user);
		return userDTO;
	}
	
	public UserDTO getByUserID(String userID) {
		UserDTO userDTO = null;
		User user = this.userDAO.getByUserID(userID);
		if (user != null)
			userDTO = new UserDTO(user);
		return userDTO;
	}

	public UserDTO create(User user) {
		return new UserDTO(this.userDAO.save(user));
	}
	
}
