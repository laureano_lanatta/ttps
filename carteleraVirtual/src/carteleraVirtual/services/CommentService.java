package carteleraVirtual.services;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import carteleraVirtual.model.Comment;
import carteleraVirtual.model.dao.CommentDAO;

@Service
@Transactional
public class CommentService {

	@Autowired
	private CommentDAO commentDAO;
	
	public List<Comment> getAll() {
		return this.commentDAO.getAll();
	}
	
	public Comment get(long id) {
		return this.commentDAO.get(id);
	}
	
	public Comment create(Comment comment) {
		return this.commentDAO.save(comment);
	}
	
}
