package carteleraVirtual.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicUpdate;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "user_credential")
public class UserCredential {

	@Id
	@GeneratedValue
	protected Long id;
	
	protected String userID;
	
	protected String userPWD;
	
	public UserCredential() {
		
	}
	
	public UserCredential(String userID, String userPWD) {
		this.userID = userID;
		this.userPWD = userPWD;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String UserID) {
		this.userID = UserID;
	}

	public String getUserPWD() {
		return userPWD;
	}

	public void setUserPWD(String UserPWD) {
		this.userPWD = UserPWD;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.getId() + " UserID: " + this.getUserID() + " UserPWD: " + this.getUserPWD();
	}
	
	@Override
	public boolean equals(Object o) {
		return o != null && 
				((UserCredential)o).getUserID().equals(this.getUserID()) &&
				((UserCredential)o).getUserPWD().equals(this.getUserPWD());
	}

}
