package carteleraVirtual.model;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

public class Notification {

	private User user;
	private Date date;
	private String body;
	
	public Notification(User user, String someText) {
		this.user = user;
		this.date = new Date();
		this.body = someText;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
}
