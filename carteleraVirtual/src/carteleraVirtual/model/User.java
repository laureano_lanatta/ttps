package carteleraVirtual.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String userID;
	
	@Column(nullable = false)
	private String username;
	
	@ManyToOne
	private Profile profile;
	
	@OneToMany(mappedBy = "author")
	private Set<Comment> comments = new HashSet<Comment>();
	
	@ManyToMany
	@JoinTable(name = "user_following",
		joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "billboard_id", referencedColumnName = "id"))
	private Set<Billboard> following = new HashSet<Billboard>();
	
	@ManyToMany
	@JoinTable(name = "user_pending_reading",
		joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "billboard_id", referencedColumnName = "id"))
	private Set<Billboard> pendingReading = new HashSet<Billboard>();

	@ManyToMany
	@JoinTable(name = "user_authorized",
		joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "billboard_id", referencedColumnName = "id"))
	private Set<Billboard> authorized = new HashSet<Billboard>();
	
	@OneToMany(mappedBy = "user")
	private Set<NotificationMethod> notificationMethods = new HashSet<NotificationMethod>();
	
	@OneToMany(mappedBy = "author")
	private Set<Post> posts = new HashSet<Post>();	
	
	public User() {
		
	}
	
	public User(String userID, String username, Profile profile) {
		this();
		this.userID = userID;
		this.username = username;
		this.profile = profile;
	}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Billboard> getFollowing() {
		return following;
	}

	public void setFollowing(Set<Billboard> following) {
		this.following = following;
	}

	public Set<Billboard> getPendingReading() {
		return pendingReading;
	}

	public void setPendingReading(Set<Billboard> pendingReading) {
		this.pendingReading = pendingReading;
	}
	
	public Set<Billboard> getAuthorized() {
		return authorized;
	}

	public void setAuthorized(Set<Billboard> authorized) {
		this.authorized = authorized;
	}
	
	public Set<NotificationMethod> getNotificationMethods() {
		return notificationMethods;
	}

	public void setNotificationMethods(Set<NotificationMethod> notificationMethods) {
		this.notificationMethods = notificationMethods;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	@Override
	public boolean equals(Object o) {
		return o != null
				&& ((User)o).getId().equals(this.getId());
	}
	
}
