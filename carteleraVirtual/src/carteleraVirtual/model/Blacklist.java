package carteleraVirtual.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

//@Component
public class Blacklist {

	private final Map<String, Date> blacklistedTokens = new HashMap<String, Date>();
	
	private Runnable cleanExpired = new Runnable() {
	    public void run() {
	    	blacklistedTokens.forEach((uuid, expirationDate) -> removeExpiredToken(uuid, expirationDate));
	    }
	};
	
	@PostConstruct
	private void startCleaning() {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(cleanExpired, 0, 900, TimeUnit.SECONDS);
	}
	
	private void removeExpiredToken(String uuid, Date expirationDate) {
		if (expirationDate.after(new Date()))
			this.blacklistedTokens.remove(uuid);
	}
	
	public void add(Jws<Claims> token) {
		this.blacklistedTokens.put(token.getBody().getId(), token.getBody().getExpiration());
	}
	
	public boolean contains(Jws<Claims> token) {
		return this.blacklistedTokens.containsKey(token.getBody().getId());
	}
	
}
