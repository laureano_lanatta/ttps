package carteleraVirtual.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comment")
public class Comment {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private User author;
	
	@ManyToOne
	private Post post;
	
	@OneToOne
	private Comment responding;
	
	private Date commentedAt = new Date();
	
	private String body;
	
	public Comment() {
		
	}

	public Comment(User aUser, Post aPost, String aBody) {
		this();
		this.author = aUser;
		this.post = aPost;
		this.body = aBody;
	}
	
	public Comment(User aUser, Post aPost, String aBody, Comment responding) {
		this(aUser, aPost, aBody);
		this.responding = responding;
	}	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	public Date getCommentedAt() {
		return commentedAt;
	}

	public void setCommentedAt(Date commentedAt) {
		this.commentedAt = commentedAt;
	}

	public Comment getResponding() {
		return responding;
	}

	public void setResponding(Comment responding) {
		this.responding = responding;
	}
	
	@Override
	public boolean equals(Object o) {
		return o != null && ((Comment)o).getId().equals(this.getId());
	}
	
}
