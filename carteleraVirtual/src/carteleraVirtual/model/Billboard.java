package carteleraVirtual.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "billboard")
public class Billboard {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToMany(mappedBy = "authorized")
	private Set<User> authorized = new HashSet<User>();
	
	@ManyToMany(mappedBy = "following")
	private Set<User> followers = new HashSet<User>();
	
	@OneToMany(mappedBy = "billboard")
	private Set<Post> posts = new HashSet<Post>();
	
	@ManyToMany(mappedBy = "pendingReading")
	private Set<User> pendingReading = new HashSet<User>();
	
	@ManyToMany
	private Set<Tag> tags = new HashSet<Tag>();
	
	private String name;

	public Billboard() {
		
	}
	
	public Billboard(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getAuthorized() {
		return authorized;
	}

	public void setAuthorized(Set<User> authorized) {
		this.authorized = authorized;
	}

	public Set<User> getFollowers() {
		return followers;
	}

	public void setFollowers(Set<User> followers) {
		this.followers = followers;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
	
	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}
	
	public Set<User> getPendingReading() {
		return pendingReading;
	}

	public void setPendingReading(Set<User> pendingReading) {
		this.pendingReading = pendingReading;
	}

	public void follow(User aUser) {
		
	}
	
	public void unfollow(User aUser) {
		
	}
	
	public void post(Post aPost) {
		
	}
	
	public void removePost(Post aPost) {
		
	}
	
	public void authorize(User aUser) {
		
	}
	
	public void deauthorize(User aUser) {
		
	}
	
	public void addTag(Tag aTag) {
		this.tags.add(aTag);
	}
	
	public void removeTag(Tag aTag) {
		this.tags.remove(aTag);
	}
	
	@Override
	public boolean equals(Object o) {
		return o != null && ((Billboard)o).getId().equals(this.getId());
	}
	
}
