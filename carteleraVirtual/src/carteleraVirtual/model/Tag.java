package carteleraVirtual.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tag")
public class Tag {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToMany(mappedBy = "tags")
	private Set<Billboard> billboards;
	
	private String name;
	
	public Tag() {
		
	}
	
	public Tag(String aName) {
		this.name = aName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Billboard> getBillboards() {
		return billboards;
	}

	public void setBillboards(Set<Billboard> billboards) {
		this.billboards = billboards;
	}
	
	public void addBillboard(Billboard aBillboard) {
		this.billboards.add(aBillboard);
	}
	
	public void removeBillboard(Billboard aBillboard) {
		this.billboards.remove(aBillboard);
	}
	
	@Override
	public boolean equals(Object o) {
		return o != null && ((Tag)o).getId().equals(this.getId());
	}
	
}
