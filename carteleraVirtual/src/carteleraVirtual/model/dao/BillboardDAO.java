package carteleraVirtual.model.dao;

import carteleraVirtual.model.Billboard;

public interface BillboardDAO extends GenericDAO<Billboard> {

	public Billboard getByName(String name);
	
}
