package carteleraVirtual.model.dao;

import carteleraVirtual.model.UserCredential;

public interface UserCredentialDAO extends GenericDAO<UserCredential> {

	public UserCredential getByIdAndPwd(String userID, String userPWD);
	
}
