package carteleraVirtual.model.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface GenericDAO<T> {

	public T get(Serializable id);
	public List<T> getAll();
	public boolean exists(Serializable id);
	public T save(T entity);
	public T update(T entity);
	public T delete(T entity);
	public T delete(Serializable id);
		
}
