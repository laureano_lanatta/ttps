package carteleraVirtual.model.dao;

import carteleraVirtual.model.Post;

public interface PostDAO extends GenericDAO<Post> {

	public Post getByTitle(String title);
	
}
