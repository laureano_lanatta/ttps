package carteleraVirtual.model.dao.impl;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import carteleraVirtual.model.Billboard;
import carteleraVirtual.model.Tag;
import carteleraVirtual.model.dao.TagDAO;

@Repository
public class TagDAOHibernateJPA extends GenericDAOHibernateJPA<Tag> implements TagDAO {

	public TagDAOHibernateJPA() {
		super(Tag.class);
	}

	@Override
	public Tag getByName(String name) {
		String q = "FROM Tag t WHERE t.name = :name";
		TypedQuery<Tag> query = this.getEntityManager().createQuery(q, Tag.class)
												  	   .setParameter("name", name)
												  	   .setMaxResults(1);
		return query.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
	}

}
