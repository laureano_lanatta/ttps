package carteleraVirtual.model.dao.impl;

import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import carteleraVirtual.model.Profile;
import carteleraVirtual.model.dao.ProfileDAO;

@Repository
public class ProfileDAOHibernateJPA extends GenericDAOHibernateJPA<Profile> implements ProfileDAO {

	public ProfileDAOHibernateJPA() {
		super(Profile.class);
	}
	
	@Override
	public Profile getByName(String name) {
		String q = "FROM Profile p WHERE p.name = :name";
		TypedQuery<Profile> query = this.getEntityManager().createQuery(q, Profile.class)
				 										   .setParameter("name", name)
				 										   .setMaxResults(1);
		return query.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
	}

}
