package carteleraVirtual.model.dao.impl;

import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import carteleraVirtual.model.UserCredential;
import carteleraVirtual.model.dao.UserCredentialDAO;

@Repository
public class UserCredentialDAOHibernateJPA extends GenericDAOHibernateJPA<UserCredential> implements UserCredentialDAO {

	public UserCredentialDAOHibernateJPA() {
		super(UserCredential.class);
	}
	
	@Override
	public UserCredential getByIdAndPwd(String userID, String userPWD) {
		String q = "FROM UserCredential c WHERE c.userID = :userID AND c.userPWD = :userPWD";
		TypedQuery<UserCredential> query = this.getEntityManager().createQuery(q, UserCredential.class)
																  .setParameter("userID", userID)
																  .setParameter("userPWD", userPWD)
																  .setMaxResults(1);
		return query.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
	}
	
}
