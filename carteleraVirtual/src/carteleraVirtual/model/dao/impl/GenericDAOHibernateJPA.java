package carteleraVirtual.model.dao.impl;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import carteleraVirtual.model.dao.GenericDAO;

public abstract class GenericDAOHibernateJPA<T> implements GenericDAO<T> {

	@PersistenceContext
	private EntityManager entityManager;
	
	protected Class<T> persistentClass;
	
	public void setEntityManager(EntityManager entityManager){
		this.entityManager = entityManager;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public GenericDAOHibernateJPA(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}
	
	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	public void setPersistentClass(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	@Override
	public T get(Serializable id) {
		return this.getEntityManager().find(this.getPersistentClass(), id);
	}

	@Override
	public List<T> getAll() {
		String query = "FROM " + this.getPersistentClass().getSimpleName();
		return this.getEntityManager().createQuery(query, this.getPersistentClass()).getResultList();
	}
	
	@Override
	public boolean exists(Serializable id) {
		return this.get(id) != null;
	}

	@Override
	public T save(T entity) {
		return this.getEntityManager().merge(entity);
	}

	@Override
	public T update(T entity) {
		return this.save(entity);
	}

	@Override
	public T delete(T entity) {
		final T e = entity;
		this.getEntityManager().remove(this.getEntityManager().contains(entity) ? entity
																	  			: this.getEntityManager().merge(entity));
		return e;
	}

	@Override
	public T delete(Serializable id) {
		//T entity = this.get(id);
		//this.delete(entity);
		return this.delete(this.get(id));
	}

}
