package carteleraVirtual.model.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import carteleraVirtual.model.Comment;
import carteleraVirtual.model.Post;
import carteleraVirtual.model.User;
import carteleraVirtual.model.dao.CommentDAO;

@Repository
public class CommentDAOHibernateJPA extends GenericDAOHibernateJPA<Comment> implements CommentDAO {

	public CommentDAOHibernateJPA() {
		super(Comment.class);
	}

	@Override
	public List<Comment> getByAuthor(User user) {
		String q = "FROM Comment c WHERE c.author = :author";
		TypedQuery<Comment> query = this.getEntityManager().createQuery(q, Comment.class)
													 	   .setParameter("author", user);
		return query.getResultList();
	}

	@Override
	public List<Comment> getByPost(Post post) {
		String q = "FROM Comment c WHERE c.post = :post";
		TypedQuery<Comment> query = this.getEntityManager().createQuery(q, Comment.class)
													 	   .setParameter("post", post);
		return query.getResultList();
	}

}
