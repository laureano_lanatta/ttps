package carteleraVirtual.model.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import carteleraVirtual.model.User;
import carteleraVirtual.model.dao.UserDAO;

@Repository
public class UserDAOHibernateJPA extends GenericDAOHibernateJPA<User> implements UserDAO {

	public UserDAOHibernateJPA() {
		super(User.class);
	}
	
	@Override
	public User getByUserID(String userID) {
		String q = "FROM User u WHERE u.userID = :userID";
		TypedQuery<User> query = this.getEntityManager().createQuery(q, User.class)
											  			.setParameter("userID", userID)
											  			.setMaxResults(1);
		return query.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
	}

	@Override
	public User getByUsername(String username) {
		String q = "FROM User u WHERE u.username = :username";
		TypedQuery<User> query = this.getEntityManager().createQuery(q, User.class)
												  		.setParameter("username", username)
												  		.setMaxResults(1);
		return query.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
	}

}
