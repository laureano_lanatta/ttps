package carteleraVirtual.model.dao.impl;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import carteleraVirtual.model.Billboard;
import carteleraVirtual.model.User;
import carteleraVirtual.model.dao.BillboardDAO;

@Repository
public class BillboardDAOHibernateJPA extends GenericDAOHibernateJPA<Billboard> implements BillboardDAO {

	public BillboardDAOHibernateJPA() {
		super(Billboard.class);
	}

	@Override
	public Billboard getByName(String name) {
		String q = "FROM Billboard b WHERE b.name = :name";
		TypedQuery<Billboard> query = this.getEntityManager().createQuery(q, Billboard.class)
															 .setParameter("name", name)
															 .setMaxResults(1);
		return query.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
	}

}
