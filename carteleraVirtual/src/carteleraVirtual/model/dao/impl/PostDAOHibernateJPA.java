package carteleraVirtual.model.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import carteleraVirtual.model.Billboard;
import carteleraVirtual.model.Post;
import carteleraVirtual.model.dao.PostDAO;

@Repository
public class PostDAOHibernateJPA extends GenericDAOHibernateJPA<Post> implements PostDAO {

	public PostDAOHibernateJPA() {
		super(Post.class);
	}

	@Override
	public Post getByTitle(String title) {
		String q = "FROM Post p WHERE p.title = :title";
		TypedQuery<Post> query = this.getEntityManager().createQuery(q, Post.class)
												  		.setParameter("title", title)
												  		.setMaxResults(1);
		return query.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
	}
	
}
