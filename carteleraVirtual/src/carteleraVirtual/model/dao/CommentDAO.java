package carteleraVirtual.model.dao;

import java.util.List;

import carteleraVirtual.model.Comment;
import carteleraVirtual.model.Post;
import carteleraVirtual.model.User;

public interface CommentDAO extends GenericDAO<Comment> {

	public List<Comment> getByAuthor(User user);
	public List<Comment> getByPost(Post post);
	
}
