package carteleraVirtual.model.dao;

import carteleraVirtual.model.Tag;

public interface TagDAO extends GenericDAO<Tag> {

	public Tag getByName(String name);
	
}
