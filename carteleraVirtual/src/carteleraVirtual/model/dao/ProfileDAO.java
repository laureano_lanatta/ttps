package carteleraVirtual.model.dao;

import carteleraVirtual.model.Profile;

public interface ProfileDAO extends GenericDAO<Profile> {

	public Profile getByName(String name);
	
}
