package carteleraVirtual.model.dao;

import carteleraVirtual.model.User;

public interface UserDAO extends GenericDAO<User> {

	public User getByUserID(String userID);
	public User getByUsername(String username);
	
}
