package carteleraVirtual.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "post_body")
public class PostBody {
	
	@Id @GeneratedValue
	private Long id;
	
	@OneToOne(mappedBy = "body")
	private Post post;
	
	private String text;
	private String imgHref;
	private String videoHref;
	
	public PostBody() {
		
	}
	
	public PostBody(Post aPost) {
		this.post = aPost;
	}
	
	public PostBody(Post aPost, String someText, String anImgHref, String aVideoHref) {
		this(aPost);
		this.text = someText;
		this.imgHref = anImgHref;
		this.videoHref = aVideoHref;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post aPost) {
		this.post = aPost;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImgHref() {
		return imgHref;
	}

	public void setImgHref(String imgHref) {
		this.imgHref = imgHref;
	}

	public String getVideoHref() {
		return videoHref;
	}

	public void setVideoHref(String videoHref) {
		this.videoHref = videoHref;
	}
	
}
