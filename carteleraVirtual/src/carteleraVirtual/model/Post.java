package carteleraVirtual.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "post")
public class Post {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne
	private Billboard billboard;
	
	@ManyToOne
	private User author;
	
	@OneToOne
	private PostBody body;
	
	@OneToMany(mappedBy = "post")
	private Set<Comment> comments;
	
	private String title;
	
	private Date date;
	
	private boolean allowComments;
	
	public Post() {
		
	}
	
	public Post(Billboard aBillboard, User aUser, Set<String> inputData) {
		this.billboard = aBillboard;
		this.author = aUser;
		this.body = new PostBody(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Billboard getBillboard() {
		return billboard;
	}
	
	public void setBillboard(Billboard billboard) {
		this.billboard = billboard;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PostBody getBody() {
		return body;
	}

	public void setBody(PostBody body) {
		this.body = body;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public boolean isAllowComments() {
		return allowComments;
	}

	public void setAllowComments(boolean allowComments) {
		this.allowComments = allowComments;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
	public void comment(User aUser, Comment aComment) {
		
	}
	
	public void removeComment(Comment aComment) {
		
	}
	
	@Override
	public boolean equals(Object o) {
		return o != null && ((Post)o).getId().equals(this.getId());
	}
	
}
