package carteleraVirtual.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("by_email")
public class NotifyByEmail extends NotificationMethod {

	@Override
	public void getNotified() {

	}

}
