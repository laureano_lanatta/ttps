package carteleraVirtual.model.dto;

import carteleraVirtual.model.Profile;

public class ProfileDTO {

	private long id;
	private String name;
	
	public ProfileDTO(Profile profile) {
		this.id = profile.getId();
		this.name = profile.getName();
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
		
}
