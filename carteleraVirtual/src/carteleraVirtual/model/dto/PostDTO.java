package carteleraVirtual.model.dto;

import carteleraVirtual.model.Post;

public class PostDTO {

	private long id;
	private long billboard_id;
	private String billboard_name;
	private long author_id;
	private String author_name;
	private PostBodyDTO post_body;
	private String title;
	private String date;
	private boolean allow_comments;
	
	public PostDTO(Post post) {
		this.id = post.getId();
		this.billboard_id = post.getBillboard().getId();
		this.billboard_name = post.getBillboard().getName();
		this.author_id = post.getAuthor().getId();
		this.author_name = post.getAuthor().getUsername();
		this.post_body = new PostBodyDTO(post.getBody());
		this.title = post.getTitle();
		this.date = post.getDate().toString();
		this.allow_comments = post.isAllowComments();
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getBillboard_id() {
		return billboard_id;
	}
	
	public void setBillboard_id(long billboard_id) {
		this.billboard_id = billboard_id;
	}
	
	public String getBillboard_name() {
		return billboard_name;
	}
	
	public void setBillboard_name(String billboard_name) {
		this.billboard_name = billboard_name;
	}
	
	public long getAuthor_id() {
		return author_id;
	}
	
	public void setAuthor_id(long author_id) {
		this.author_id = author_id;
	}
	
	public String getAuthor_name() {
		return author_name;
	}
	
	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}
	
	public PostBodyDTO getPost_body() {
		return post_body;
	}
	
	public void setPost_body(PostBodyDTO post_body) {
		this.post_body = post_body;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public boolean isAllow_comments() {
		return allow_comments;
	}
	
	public void setAllow_comments(boolean allow_comments) {
		this.allow_comments = allow_comments;
	}
	
}
