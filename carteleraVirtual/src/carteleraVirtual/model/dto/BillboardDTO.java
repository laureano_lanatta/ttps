package carteleraVirtual.model.dto;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import carteleraVirtual.model.Billboard;

public class BillboardDTO {

	private long id;
	private String name;
	private Set<String> tags;
	
	public BillboardDTO(Billboard billboard) {
		this.id = billboard.getId();
		this.name = billboard.getName();
		this.tags = billboard.getTags()
							 .stream()
							 .map(tag -> String.format("/carteleraVirtual/tags/%d", tag.getId()))
							 .collect(Collectors.toSet());
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	
}
