package carteleraVirtual.model.dto;

import carteleraVirtual.model.PostBody;

public class PostBodyDTO {

	private long id;
	private String text;
	private String img_href;
	private String video_href;
	
	public PostBodyDTO(PostBody postBody) {
		this.id = postBody.getId();
		this.text = postBody.getText();
		this.img_href = postBody.getImgHref();
		this.video_href = postBody.getVideoHref();
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getImg_href() {
		return img_href;
	}
	
	public void setImg_href(String img_href) {
		this.img_href = img_href;
	}
	
	public String getVideo_href() {
		return video_href;
	}
	
	public void setVideo_href(String video_href) {
		this.video_href = video_href;
	}		
	
}
