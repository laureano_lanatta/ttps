package carteleraVirtual.model.dto;

import carteleraVirtual.model.Comment;

public class CommentDTO {

	private long id;
	private long author_id;
	private String author_name;
	private long post_id;
	private String post_title;
	private long responding_id;
	private String commented_at;
	private String body;
	
	public CommentDTO(Comment comment) {
		this.id = comment.getId();
		this.author_id = comment.getAuthor().getId();
		this.author_name = comment.getAuthor().getUsername();
		this.post_id = comment.getPost().getId();
		this.post_title = comment.getPost().getTitle();
		this.commented_at = comment.getCommentedAt().toString();
		this.body = comment.getBody();
		
		Comment respondingTo = comment.getResponding();
		if (respondingTo != null)
			this.responding_id = comment.getResponding().getId();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAuthor_id() {
		return author_id;
	}

	public void setAuthor_id(long author_id) {
		this.author_id = author_id;
	}

	public String getAuthor_name() {
		return author_name;
	}

	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}

	public long getPost_id() {
		return post_id;
	}

	public void setPost_id(long post_id) {
		this.post_id = post_id;
	}

	public String getPost_title() {
		return post_title;
	}

	public void setPost_title(String post_title) {
		this.post_title = post_title;
	}

	public long getResponding_id() {
		return responding_id;
	}

	public void setResponding_id(long responding_id) {
		this.responding_id = responding_id;
	}

	public String getCommented_at() {
		return commented_at;
	}

	public void setCommented_at(String commented_at) {
		this.commented_at = commented_at;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
}
