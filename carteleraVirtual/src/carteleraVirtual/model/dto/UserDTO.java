package carteleraVirtual.model.dto;

import carteleraVirtual.model.User;

public class UserDTO {

	private Long id;
	private String user_id;
	private String username;
	private String profile;
	
	public UserDTO(User user) {
		this.id = user.getId();
		this.user_id = user.getUserID();
		this.username = user.getUsername();
		this.profile = user.getProfile().getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
}
