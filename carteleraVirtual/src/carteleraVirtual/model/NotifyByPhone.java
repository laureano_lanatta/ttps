package carteleraVirtual.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("by_phone")
public class NotifyByPhone extends NotificationMethod {

	@Override
	public void getNotified() {
		
	}

}
