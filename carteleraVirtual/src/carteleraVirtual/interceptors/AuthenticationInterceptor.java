package carteleraVirtual.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import carteleraVirtual.annotations.AuthenticationRequired;
import carteleraVirtual.config.Environment;
import carteleraVirtual.model.Blacklist;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

//@Component
public class AuthenticationInterceptor implements HandlerInterceptor {
	
	//@Autowired
	private Blacklist blacklist;
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		AuthenticationRequired authenticationRequired = handlerMethod.getMethod().getAnnotation(AuthenticationRequired.class);
		if (authenticationRequired != null) {
			try {
				String authorization = request.getHeader("authorization");
				String jws = authorization.replace("Bearer ", "");
				Jws<Claims> token = Jwts.parser()
						 				.setSigningKey(Environment.KEY)
						 				.parseClaimsJws(jws);
				if (this.blacklist.contains(token))
					throw new Exception("Token invalidated");
			} catch (Exception e) {
				response.setStatus(401);
				return false;
			}
		}
		return true;
	}
	
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			@Nullable ModelAndView modelAndView) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		AuthenticationRequired authenticationRequired = handlerMethod.getMethod().getAnnotation(AuthenticationRequired.class);
		if (authenticationRequired != null) {
			String authorization = request.getHeader("authorization");			
			if (authorization != null) {
				String jws = authorization.replace("Bearer ", "");
				Jws<Claims> token = Jwts.parser()
										 .setSigningKey(Environment.KEY)
										 .parseClaimsJws(jws);
				if (request.getHeader("revoke") != null)
					this.blacklist.add(token);
			}
		}
		//response.addHeader("Access-Control-Expose-Headers", "Authorization");
		//response.addHeader("Access-Control-Allow-Origin", "*");
	}
	
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			@Nullable Exception ex) throws Exception {
		
	}
	
}
