package ttps.clasificados;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Encabezado")
public class Encabezado extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private SitioClasificado info;
	
	public void init() throws ServletException {
		info = (SitioClasificado)this.getServletContext().getAttribute("info");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("<h3>");
		out.print(info.getNombre() + " - ");
		out.print(info.getEmail() + " - ");
		out.print(info.getTelefono());
		out.println("</h3>");
	}

}
