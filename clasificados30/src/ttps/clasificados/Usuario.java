package ttps.clasificados;

public class Usuario {

	private String name;
	private String pass;
	private String profile;
	
	public Usuario(String name, String pass) {
		this.name = name;
		this.pass = pass;
	}
	
	public Usuario(String name, String pass, String profile) {
		this(name, pass);
		this.profile = profile;
	}
	
	public String getName() {
		return name;
	}

	
	private String getPass() {
		return pass;
	}
	
	public String getProfile() {
		return profile;
	}
	
	public boolean equals(Object u) {
		return this.getName().equals(((Usuario)u).getName())
				&& this.getPass().equals(((Usuario)u).getPass());
	}
	
}
