package ttps.clasificados;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Login")
public class Login extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private List<Usuario> usuarios;
	
	public void init() throws ServletException {
		usuarios = new ArrayList<Usuario>();
		usuarios.add(new Usuario("a", "1", "admin"));
		usuarios.add(new Usuario("b", "2", "publi"));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("usuario");
		String pass = request.getParameter("password");
		Usuario usr = new Usuario(name, pass);
		int index = usuarios.indexOf(usr);
		
		//Con sendRedirect
//		if (index < 0)
//			response.sendRedirect("error.html");
//		else
//			if (usuarios.get(index).getProfile().equals("admin"))
//				response.sendRedirect("administrador.html");
//			else
//				response.sendRedirect("publicador.html");
		
		//Con forward y otro servlet (Menu)
		if (index < 0)
			response.sendRedirect("error.html");
		else {
			RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Menu");
			if (dispatcher != null) {
				request.setAttribute("profile", usuarios.get(index).getProfile());
				dispatcher.forward(request, response);
			}
		}
	}

}
