package ttps.clasificados;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class InfoSitio implements ServletContextListener {

    public InfoSitio() {
    	
    }

    public void contextDestroyed(ServletContextEvent event)  {
    	
    }

    public void contextInitialized(ServletContextEvent event)  { 
         SitioClasificado info = new SitioClasificado();
         info.setNombre(event.getServletContext().getInitParameter("nombre"));
         info.setEmail(event.getServletContext().getInitParameter("email"));
         info.setTelefono(event.getServletContext().getInitParameter("telefono"));
         event.getServletContext().setAttribute("info", info);
    }
	
}
