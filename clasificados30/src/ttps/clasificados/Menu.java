package ttps.clasificados;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Menu")
public class Menu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Encabezado");
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		
		if (dispatcher != null)
			dispatcher.include(request, response);
		
		if (request.getAttribute("profile").equals("admin")) {
			out.println("<h1>Administrador</h1>");
			out.println("<ul>");
			out.println("<li><a href=#>Actualizar datos de contacto</a></li>");
			out.println("<li>Administradores");
			out.println("<ul>");
			out.println("<li><a href=#>Agragar</a></li>");
			out.println("<li><a href=#>Eliminar</a></li>");
			out.println("<li><a href=#>Modificar</a></li>");
			out.println("</ul>");
			out.println("</li>");
			out.println("<li><a href=#>Contestar consultas</a></li>");
			out.println("</ul>");
		} else {
			out.println("<h1>Publicador</h1>");
			out.println("<ul>");
			out.println("<li><a href=#>Listar usuarios publicadores</a></li>");
			out.println("<li>Publicaciones");
			out.println("<ul>");
			out.println("<li><a href=#>Agragar</a></li>");
			out.println("<li><a href=#>Eliminar</a></li>");
			out.println("<li><a href=#>Modificar</a></li>");
			out.println("</ul>");
			out.println("</li>");
			out.println("<li><a href=#>Ver estadisticas</a></li>");
			out.println("</ul>");
		}
		out.println("</body></html>");
		out.close();
	}

}
