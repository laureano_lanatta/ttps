package filters;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter("/*")
public class FiltroLogDeAccesos implements Filter {
	
	FilterConfig config;
	
	public void init(FilterConfig fConfig) throws ServletException {
		this.config = fConfig;
	}
	
	public void destroy() {
		this.config = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		
		String ipAddr = req.getHeader("host");
		String timeDate = new Date().toString();
		String method = req.getMethod();
		String uri = req.getRequestURI();
		String protocol = req.getProtocol();
		String userAgent = req.getHeader("user-agent");
		String log = ipAddr + " - "
					 + timeDate + " \""
					 + method + " "
					 + uri + " "
					 + protocol + "\" "
					 + userAgent;
		
		config.getServletContext().log(log);
		
		chain.doFilter(request, response);
	}

}
