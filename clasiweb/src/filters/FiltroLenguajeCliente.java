package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter("/*")
public class FiltroLenguajeCliente implements Filter {

	FilterConfig config;
	
	public void init(FilterConfig fConfig) throws ServletException {
		this.config = fConfig;
	}
	
	public void destroy() {
		this.config = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		String lang = req.getHeader("accept-language").substring(0, 2);
		if (lang.equals("es"))
			config.getServletContext().setAttribute("langFile", "textos_es");
		else
			config.getServletContext().setAttribute("langFile", "textos_en");
		chain.doFilter(request, response);
	}

}
