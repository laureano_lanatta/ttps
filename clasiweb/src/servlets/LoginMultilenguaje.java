package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ResourceBundle;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/LoginMultilenguaje")
public class LoginMultilenguaje extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ResourceBundle rb;
	
	public void init() throws ServletException {
		String langFilePath = "resources/" + this.getServletContext().getAttribute("langFile");
		rb = ResourceBundle.getBundle(langFilePath);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println("<h1>" + rb.getString("titulo") + "</h1>");
		out.println("<form action=\"Login\" method=\"post\">");
		out.println("<label>" + rb.getString("labelusuario") + "</label>");
		out.println("<input type=\"text\" name=\"username\" /><br/>");
		out.println("<label>" + rb.getString("labelpassword") + "</label>");
		out.println("<input type=\"password\" name=\"userpassword\" /><br/>");
		out.println("<input type=\"submit\" value=\"Enviar\" />");
		out.println("</form>");
		out.print("</body></html>");
		out.close();
	}

}
